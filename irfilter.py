#!/usr/bin/env python
#
# Written with help from
# https://business.tutsplus.com/tutorials/controlling-dc-motors-using-python-with-a-raspberry-pi--cms-20051
#
# Rpi GPIO/wiring pi/piwedge mapping can be obtained with the 
# gpio readall 
# command.  The piwedge uses the BCM pin numbers and wiring pi uses wPi
# (surprise)
# For example:
# Motor1A = 21  # wiring pi 21, maps to physical pin 29/pi wedge g5
#
# This is written for a SN754410 motor controller to enable and disable an IR cut
# filter.  It's got a solenoid so it's similar to a motor.  We'll just give it
# the minimum pulse length to make it work.  Unlike a stepper motor or regular
# motor, it only requires one little pulse and it's done.  
# 
# SN754410 to RPi pin connections:
# 1: Enable output on high: G18 on piwedge, wiring pi pin 1,  tutorial is GPIO25/Phys 22
# 2: driver input:          G19 on piwedge, wiring pi pin 24, tutorial is GPIO24/Phys 18
# 3: cut filter wire one
# 4: Ground
# 5: Ground
# 6: cut filter wire one
# 7: driver input:          G20 on piwedge, wiring pi pin 28, tutorial is GPIO23/Phys 16
# 12: Ground
# 13: Ground
# 8: Vcc, 5v power for solenoid of Rpi
# 16: Vcc, 5v logic power supply of Rpi
# 
#
import wiringpi
import signal
import sys
from time import sleep

#wiringpi.wiringPiSetupPhys()

#
# Pin modes.
# 
#
INPUT  = 0
OUTPUT = 1

HIGH = 1
LOW = 0

PUD_DIS  = 0
PUD_DOWN = 1
PUD_UP   = 2


class FilterDriver:
    def __init__(self, PinA, PinB, EnablePin, **keyword_parameters):
        self.filterPins = [PinA, PinB, EnablePin]
        self.filterON   = [HIGH, LOW, HIGH]
        self.filterOFF  = [LOW, HIGH, HIGH]
        self.enabled = False
        self.delay   = keyword_parameters.pop('delay', 20)

        ##
        # Allow a user to invert the in/out sequence.  Not everyone will plug
        # the positive into the right pin (whatever that is)
        ##
        if ('invert' in keyword_parameters):
            self.filterOFF = [HIGH, LOW, HIGH]
            self.filterON  = [LOW, HIGH, HIGH]
        ##
        # This is kind of lame but wiring pi can only be called once with the
        # python libraries that I'm using.  There is another python binding that
        # avoids this issue, but it's poorly maintained.  The time required to
        # debug this poorly maintained python wiringPi binding isn't worth
        # it so we'll just have this optional setup parameter.
        ##
        if ('setupPi' in keyword_parameters):
            wiringpi.wiringPiSetup()
        ##
        # Enable the pins 
        ##
        self.filterEnable()

    def cleanup(self):
        for pin in self.filterPins:
            wiringpi.digitalWrite(pin, LOW)
        self.filterDisable();

    def filterEnable(self):
        for pin in self.filterPins:
            wiringpi.pinMode(pin, OUTPUT)
            wiringpi.pullUpDnControl(pin, PUD_DOWN)
        self.enabled = True

    ## 
    # This may not make sense.  Setting pin mode to INPUT
    # sometimes produces an undefined output.
    ##
    def filterDisable(self):
        for pin in self.filterPins:
            wiringpi.pinMode(pin, INPUT)
            wiringpi.pullUpDnControl(pin, PUD_DOWN)
        self.enabled = False

    ##
    # Turn all the pins low, which should disable the output on the H bridge
    # chip
    ##
    def filterOff(self):
        for pin in self.filterPins:
            wiringpi.digitalWrite(pin, LOW)

    def filterIn(self):
        if (not self.enabled):
            self.filterEnable()

        for (pin, state) in zip(self.filterPins, self.filterON):
            wiringpi.digitalWrite(pin, state)

        wiringpi.delay(self.delay)
        self.filterOff()

    def filterOut(self):
        if (not self.enabled):
            self.filterEnable()

        for (pin, state) in zip(self.filterPins, self.filterOFF):
            wiringpi.digitalWrite(pin, state)

        wiringpi.delay(self.delay)
        self.filterOff()

    def setDelay(self, delay):
        self.delay = delay
        
        
