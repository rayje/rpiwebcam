#!/usr/bin/env python
from steppermotor import StepperDriver

Motor1A = 21  # maps to physical pin 29/pi wedge g5
Motor1B = 22  # maps to physical pin 31/pi wedge g6
Motor2A = 23  # maps to physical pin 33/pi wedge g13
Motor2B = 25  # maps to physical pin 37/pi wedge g26


stepper = StepperDriver(Motor1A, Motor1B, Motor2A, Motor2B)
stepper.cleanup()
