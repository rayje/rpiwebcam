<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Test</title>

    <link rel="stylesheet" href="style.css">
</head>
<body>
  <div id="page-wrapper">
     <h1>WebSockets Demo</h1>

     <div id="status">Connecting...</div>
    <!-- <img id="webpicture" alt=webcampicture src=./BlankWhite.jpg> -->
     <img id="webpicture" alt=webcampicture src=./campic.jpg?<?= time();?>>

     <ul id="messages"></ul>
     <form id="message-form" action="#" method="post">
     <?php
//     <textarea id="message" placeholder="Write a message here" required></textarea>
 //      <button type="submit">Send Message</button>
  //     <button type="button" id="close">Close Connection</button>
       ?>
       <table>
        <tr>
            <td><button type="button" id="filter">IR cut filter IN</button></td>
     <?php
       //     <td><button type="button" id="snap">Snap</button></td>
        //    <td><button type="button" id="freq">Frequency</button></td>
       ?>
        </tr>
        <tr>
            <td><b>Exposure:</b></td>
            <td><input type="radio" id="expDay" name="exposure" value="D">Day<br></td>
            <td><input type="radio" id="expDsk" name="exposure" value="E">Dusk<br></td>
            <td><input type="radio" id="expNit" name="exposure" value="N">Night<br></td>
        </tr>
        <tr>
            <td><b>Resolution:</b></td>
            <td><input type="radio" id="res320" name="resolution" value="320x240">320x240<br></td>
            <td><input type="radio" id="res640" name="resolution" value="640x480">640x480<br></td>
            <td><input type="radio" id="res1024" name="resolution" value="1024x768">1024x768<br></td>
            <td><input type="radio" id="res1280" name="resolution" value="1280x720">1280x720<br></td></td>
            <td><input type="radio" id="res1640" name="resolution" value="1640x1232">1640x1232<br></td>
            <td><input type="radio" id="res1920" name="resolution" value="1920x1080">1920x1080<br> </td>
            <td><input type="radio" id="res2592" name="resolution" value="2592x1944">2592x1944<br></td>
            <td><input type="radio" id="res3280" name="resolution" value="3280x2464">3280x2464<br></td>
       </tr>
       </table>

     </form>
  </div>

    <script src="app.js"></script>
</body>
</html>

