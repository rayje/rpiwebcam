#!/bin/bash

while :; do
    DSTR=$(date +%s)

    raspistill -n -w 1640 -h 1232 -a "$(date)" -o pics/${DSTR}.jpg

    ln -fs pics/${DSTR}.jpg campic.jpg
    sleep 30
done
