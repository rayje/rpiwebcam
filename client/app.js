/* trying to refresh image now
*/
window.setInterval(function()
{
  var image = document.getElementById('webpicture');
  image.src ="./campic.jpg?random="+new Date().getTime();
}, 30000); //milliseconds

window.onload = function() {
    var image = document.getElementById('webpicture');
    var form = document.getElementById('message-form');
    var messageField = document.getElementById('message');
    var messagesList = document.getElementById('messages');
    var socketStatus = document.getElementById('status');
//    var closeBtn = document.getElementById('close');
    var filterBtn = document.getElementById('filter');
//    var snapBtn = document.getElementById('snap');
//    var freqBtn = document.getElementById('freq');
    //var sizeBtn = document.getElementById('size');


    // Check support
    var support = "MozWebSocket" in window ? 'MozWebSocket' : ("WebSocket" in window ? 'WebSocket' : null);
    if (support == null) {
        alert("Your browser doesn't support Websockets.");
        return;   
    }

    // Create websocket
    //var socket = new WebSocket('ws://192.168.1.10:8888/ws');
    //var socket = new WebSocket('ws://71.229.225.0:8888/ws');
// Works   var socket = new WebSocket('wss://chipstruck.com:8888/ws');
    var socket = new WebSocket('wss://chipstruck.com/wss');

    socket.onopen = function(event) {
        console.log(event);
        socketStatus.innerHTML = 'Connected to: ' + event.currentTarget.url;
        socketStatus.className = 'open';
    };

    socket.onerror = function(error) {
        console.log('WebSocket Error: ' + error);
    };

    form.onsubmit = function(e){
        e.preventDefault();

        // Get the message from the text area
        var message = messageField.value;

        // Send the message through the WebSocket.
        socket.send(message);

        // Add the message to the messages list;
        messagesList.innerHTML += '<li class="sent"><span><Sent:</span>' +
                                    message + '</li>';

        // Clear out the message field.
        messageField.value = '';

        return false;
    };

    // Set up onclick functions for radio buttons.
    // http://stackoverflow.com/questions/8838648/onchange-event-handler-for-radio-button-input-type-radio-doesnt-work-as-one
    var exp_radios = document.getElementsByName("exposure");
    var prev = null;
    console.log("exp_Radios: " + exp_radios);
    console.log("exp_Radios, length: " + exp_radios.length);
    for (var i = 0; i < exp_radios.length; i++){
        console.log("adding radio" + i);
        exp_radios[i].addEventListener('change',
            function() {
             (prev)? console.log(prev.value):null;
                if (this !== prev) {
                    prev = this;
                }
                console.log(this.value);
                var message = 'E:' + this.value
                console.log(message)
                socket.send(message)
            }
        )
    };
    var res_radios = document.getElementsByName("resolution");
    var prev = null;
    console.log("res_Radios: " + res_radios);
    console.log("res_Radios, length: " + res_radios.length);
    for (var i = 0; i < res_radios.length; i++){
        console.log("adding radio" + i);
        res_radios[i].addEventListener('change',
            function() {
             (prev)? console.log(prev.value):null;
                if (this !== prev) {
                    prev = this;
                }
                console.log(this.value);
                var message = 'Z:' + this.value
                console.log(message)
                socket.send(message)
            }
        )
    };
       // sendMessage('E:N');

    // Set up onclick function for image
    image.onclick = function(e){
        var positions;
        var x0 = 0;
        var y0 = 0;
        var height = 0;
        var width = 0;
        var clickX = 0;
        var clickY = 0;
        var xDelta = 0; //Position relative to image center.
        var yDelta = 0; //Position relative to image center.
        var xAngle = 0;

        positions = getPosition(e.currentTarget);
        /* Calculate height and width of the image.
         * Then calculate where in the image the click occurred.
         * We want it to be relative to the center of the image so we can know
         * to move the camera to the right or left.
         */
        /* Get the height and width of the image.
         * From there calculate where in the image the click occurred.
         * Return the value as a percent relative to the center of the image.  
         * -100%...0%...100%
         *   -100%
         *    ...
         *   100%
         */

        x0 = positions.x;
        y0 = positions.y;

        height = positions.ymax;
        width = positions.xmax;

        console.log('image width: ' + width);
        console.log('image height: ' + height);

        console.log('x0: ' + x0);
        console.log('y0: ' + y0);

        clickX = e.clientX - x0;
        clickY = e.clientY - y0;

        console.log('click X position: ' + clickX)
        console.log('click Y position: ' + clickY)

        /* Calculate the click position as a percent of the image as deviation
         * from the center.
         * X percent calculations: 
         * It's 100% from middle to edge, so 200% is the total range.  We then
         * have to offset it by 100 to get to the middle
         * Xpercent = (200 * clickX/width) - 100
         * Ypercent = (200 * clickY/height) - 100
         */
        Xpercent = (200 * clickX/width) - 100
        Ypercent = (200 * clickY/height) - 100

        console.log('Xpercent: ' + Xpercent);
        console.log('Ypercent: ' + Ypercent);

//        xDelta = clickX - (width/2);
//        yDelta = clickY - (height/2);


        /* The deltas are pixel deltas from the middle of the image.
         * The field of view is 53.50 degrees, so we need to combine 
         * the resolution of the image with the field of view to get actual
         * degrees.
         * Hardcoding the resolution to 1920 for now.
         */
//        var img = document.getElementById('imageid'); 
//        var width = (e.currentTarget).clientWidth;
//        var height = (e.currentTarget).clientHeight;
//        console.log('img Width: ' + width);
//        console.log('img height: ' + height);

//        xAngle = 2 * (53.5/width) * xDelta;

//        console.log('clickX: ' + clickX);
//        console.log('clickY: ' + clickY);
//
//        console.log('yDelta: ' + yDelta);
//        console.log('xDelta: ' + xDelta);



        var message = "X:" + Xpercent + ";Y:" +  Ypercent + ";m:x0->" + x0 + "y0->" + y0;
        message += "height-> " + height + "width-> " + width;
        socket.send(message);
        console.log ("Message: " + message)
    }
    function show_exposure(value){
        var button = "expDay";
        switch(value) {
            case 'D':
                button = "expDay";
                break;
            case 'E':
                button = "expDsk";
                break;
            case 'N':
                button = "expNit";
                break;
        }
        document.getElementById(button).checked=true;
    }
    function show_resolution(value){
        var button = "res1024";
        switch(value) {
            case '3280,2464':
                button = 'res3280';
                break;
            case '2592,1944':
                button = 'res2592';
                break;
            case '1920,1080':
                button = 'res1920';
                break;
            case '1640,1232':
                button = 'res1640';
                break;
            case '1280,720':
                button = 'res1280';
                break;
            case '1024,768':
                button = 'res1024';
                break;
            case '640,480':
                button = 'res640';
                break;
            case '320,240':
                button = 'res320';
                break;
        }
        document.getElementById(button).checked=true;
    }

    function set_filter_button(val){
        // Set the IR filter button text here
        //if (filterBtn.innerHTML == "IR cut filter IN") {
        if (val == "IN"){
            console.log('Received message, changing value to "IR cut filter OUT"');
            filterBtn.innerHTML = "IR cut filter OUT";
            //var message = "F:IN";
        } else {
            console.log('Received message, changing value to "IR cut filter IN"');
            filterBtn.innerHTML = "IR cut filter IN";
            //var message = "F:OUT";
        }
    }

    /* Handle messages sent by the server
     * Later we'll add a reload once the server announces that a new image has
     * been taken.
     */
    socket.onmessage = function(event) {
        var message = event.data;
        var IR;
        var EXP;
        var statwords;
        /* Commands: 
         *  C: Connected Client Count
         *  E: Set exposure
         *  F: IR filter in or out
         *  M: Send a message
         *  Q: Set frequency of picture taking
         *  S: Snap picture
         *  X: Move in X direction
         *  Y: Move in Y direction
         *  Z: Set size (resolution) of image
         */

        statwords = message.split(";");
        if (statwords.length == 0){
            console.log("No message returned from the onmessage function!");
            return;
        }
        messagesList.innerHTML = '<li class="received"><span><Received:</span></li>';
        console.log ("Statwords:" + statwords);
        for (item in statwords) {
            console.log("Looking at " + statwords[item]);
            cmd = statwords[item].split(":");
            if (cmd.length != 2) {
                console.log ("Malformed command.  Expected <key>:<value>, got:" + word);
                return;
            }
            key = cmd[0];
            console.log("Key: " + key);
            val = cmd[1];
            console.log("Val: " + val);
            switch(key) {
                case 'E':
                    messagesList.innerHTML = '<li class="exposure"><span><Exposure:</span>' + val + '</li>'; 
                    show_exposure(val);
                    break;
                case 'F':
                    messagesList.innerHTML = '<li class="Filter:"><span><Exposure:</span>' + val + '</li>';
                    set_filter_button(val);
                    break;
                case 'Z':
                    console.log("Setting resolution control to " + val);
                    messagesList.innerHTML = '<li class="resolution"><span><Resolution:</span>' + val + '</li>'; 
                    show_resolution(val);
                    break;
            }
        }
        
        messagesList.innerHTML = '<li class="received"><span><Received:</span>' +
                                    message + '</li>';
        

    };

    // Show a disconnected message when the WebSocket is closed
    socket.onclose = function(event) {
        socketStatus.innerHTML = 'Disconnected from WebSocket.';
        socketStatus.className = 'closed';
    };

    // Generic send message function
    sendMessage = function(message) {
        console.log('filterBtn:' + filterBtn);
        console.log('sendMessage sending ' + message);
        socket.send(message);
    }

    /* Snap button 
    snapBtn.onclick = function(e) {
        // var  = document.getElementById('snap'); // Don't think this is
        // needed.
        console.log('snapBtn');
        sendMessage('S:0');
    };
    */

    // Exposure button(will be some other control later. Practicing with buttons
    // now)
    /*
    exposureBtn.onclick = function(e) {
        // var filterBtn = document.getElementById('exp');
        console.log('exposureBtn');
        sendMessage('E:N');
    };
    */

    /* Frequency button (again, will be different control later)
    freqBtn.onclick = function(e) {
        // var filterBtn = document.getElementById('freq');
        console.log('freqBtn');
        sendMessage('Q:30');
    };
    */

    // Size button
    //sizeBtn.onclick = function(e) {
        // var filterBtn = document.getElementById('size');
     //   console.log('sizeBtn');
      //  sendMessage('Z:1920x1080');
    //};


    
    // Toggle the IR cut filter and change the text to match the current state
    filterBtn.onclick = function(e) {
        var filterBtn = document.getElementById('filter');
        e.preventDefault();

        console.log('filterBtn:' + filterBtn);
        console.log('filterBtn.value: ' + filterBtn.value);
        console.log('filterBtn.innerHTML: ' + filterBtn.innerHTML);
        if (filterBtn.innerHTML == "IR cut filter IN") {
            console.log('Button click, sending F:IN');
//            filterBtn.innerHTML = "IR cut filter OUT";
            var message = "F:IN";
            socket.send(message);
        } else {
            console.log('Changing value to "IR cut filter IN"');
            console.log('Button click, sending F:OUT');
//            filterBtn.innerHTML = "IR cut filter IN";
            var message = "F:OUT";
            socket.send(message);
        }

        return false;
    };
    /* Close the WebSocket connection when the close button is clicked
    closeBtn.onclick = function(e) {
        var closeBtn = document.getElementById('close');
        e.preventDefault();

        // Close the web socket
        socket.close();

        return false;
    };
    */
    /* helper function to get an element's exact position
     * Obtained from 
     * https://www.kirupa.com/html5/get_element_position_using_javascript.htm
     */
    function getPosition(el) {
        var xPosition = 0;
        var yPosition = 0;
        var xMax = 0;
        var yMax = 0;

        if (el) {
            xMax = el.offsetWidth;
            yMax = el.offsetHeight;
        }
     
        while (el) {
            console.log('In while loop');
            if (el.tagName == "BODY") {
                console.log('tagName == BODY');
                // deal with browser quirks with body/window/document and page scroll
                var xScrollPos = el.scrollLeft || document.documentElement.scrollLeft;
                var yScrollPos = el.scrollTop || document.documentElement.scrollTop;
         
                xPosition += (el.offsetLeft - xScrollPos + el.clientLeft);
                yPosition += (el.offsetTop - yScrollPos + el.clientTop);
            } else {
                console.log('tagName != BODY');
                xPosition += (el.offsetLeft - el.scrollLeft + el.clientLeft);
                yPosition += (el.offsetTop - el.scrollTop + el.clientTop);
            }
     
            el = el.offsetParent;
      }
      return {
        x: xPosition,
        y: yPosition,
        xmax: xMax,
        ymax: yMax
      };
    }
};
