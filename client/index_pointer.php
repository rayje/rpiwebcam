<!doctype html>
<html>
<head>
  <title>Remote stepper motor control</title>
  <meta charset="utf-8" />
  <style>
    .col-centered{
    float: none;
    text-align:center;
    margin: 0 auto;
  }
  </style>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="http://code.jquery.com/jquery.min.js"></script>
  <script src="http://d3js.org/d3.v3.min.js"></script>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
  <!-- Include all compiled plugins (below), or include individual files as needed -->

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="container">
    <div class="row">
      <div id="wrapper" class="col-xs-12 col-md-6 col-lg-4 col-centered" >
        <h2>Stepper motor control</h2>
        <label id="conn_text">Connection status: Connecting <?php echo date("F j, Y, g:i a")?>!</label><br />
        <br />
      </div>
    </div>
  </div>
<script>
$(document).ready(function ()
{
  var ws = new WebSocket("ws://192.168.1.10:8888/ws");
  ws.onopen = function(evt){
    var conn_status = document.getElementById('conn_text');
    conn_status.innerHTML = "Connection status: Connected!"
  };
  ws.onclose = function(evt){
  var conn_status = document.getElementById('conn_text');
  conn_status.innerHTML = "Connection status: Disonnected!"
};

var angle = new Array();
var motor_step = 7.5;
var halfstep = 2;
var step = motor_step/halfstep;
var r1 = 100;
var r2 = 80;
var r3 = 100;
var curr_angle = 0;
var speed = 5;
var message = {"speed": speed, "angle": curr_angle}
var trans_x = 150;
var trans_y = 150;
var in_transition=0;
var RTSmessage = [];
var pointer_data = [
{"d":"M -10 0 L 0 90 L 10 0A 10 10 0 0 0-10 0","id":"pointer_ghost","a":0, "style":"opacity:0.5;"},
{"d":"M -10 0 L 0 90 L 10 0A 10 10 0 0 0-10 0","id":"pointer","a":0, "style":"opacity:1;"}];
for (i=0;i<360;i=i+step)
  angle.push(i);

var slider_label = d3.select("#wrapper")
  .append("label")
  .attr("id","slider-value")
  .html("Speed: " + speed + " s/step");

var slider = d3.select("#wrapper")
  .append("input")
  .attr("type","range")
  .attr("id","slider")
  .attr("min", 0.005)
  .attr("max", 0.5)
  .attr("step", 0.005)
  .attr("class", "col-centered")
  .attr("viewBox","0 0 300 300")
  .attr("preserveAspectRatio","xMidYMin meet")
  .on("input", function() { update(this.value);});

// Initial starting speed 
update(0.005);

function update(newSpeed) {
  // adjust the text on the range slider
  slider_label.text("Speed: " + newSpeed + " s/step");
  slider.property("value", newSpeed);
  // update the speed
  message.speed = newSpeed;
}

var svg = d3.select("#wrapper")
          .append("svg")
          .attr("id","svg1")
          .attr("viewBox","0 0 300 300")
          .attr("preserveAspectRatio","xMidYMin meet")
          .on("mousemove", click).on("click", click);

// Define the gradient
var gradient = svg.append("svg:defs")
  .append("svg:linearGradient")
  .attr("id", "gradient")
  .attr("x1", "0%")
  .attr("y1", "0%")
  .attr("x2", "100%")
  .attr("y2", "100%")
  .attr("spreadMethod", "pad");


// Define the gradient colors
gradient.append("svg:stop")
  .attr("offset", "0%")
  .attr("stop-color", "AliceBlue")
  .attr("stop-opacity", 1);

gradient.append("svg:stop")
  .attr("offset", "100%")
  .attr("stop-color", "LightBlue")
  .attr("stop-opacity", 1);

function click() {
  var position = d3.mouse(this),
  mouse_point = {"x": position[0], "y": position[1]};
  rotateElement(trans_x,trans_y,mouse_point.x,mouse_point.y);
}

//adding outter ring
var ring = svg
  .append("circle")
  .attr("cx",trans_x)
  .attr("cy",trans_y)
  .attr("r",r1+30)
  .style("fill", "url(#gradient)" )
  .style("stroke","black");

//adding outter ring – second part
var circle = svg
  .append("circle")
  .attr("cx",trans_x)
  .attr("cy",trans_y)
  .attr("r",r1)     
  .style("fill", "white")
  .style("stroke","#B90925");

//adding inner ring
var circle2 = svg
  .append("circle")
  .attr("cx",trans_x)
  .attr("cy",trans_y)
  .attr("r",r2)     
  .style("fill", "url(#gradient)") 
  .style("stroke","DarkBlue");

//adding lines
var lines = svg.selectAll("line")
  .data(angle)
  .enter()
  .append("line")
  .attr("x1",function(d){ if (d%45 == 0){ return (r2-10)*Math.cos(d* (Math.PI/180)) + trans_x;}
    else{  return (r2)*Math.cos(d* (Math.PI/180)) + trans_x;}})
  .attr("y1",function(d){ if (d%45 == 0){ return (r2-10)*Math.sin(d* (Math.PI/180)) + trans_y;}
    else {return r2*Math.sin(d* (Math.PI/180)) + trans_y;}})
  .attr("x2",function(d){ return r3*Math.cos(d* (Math.PI/180)) + trans_x})
  .attr("y2",function(d){ return r3*Math.sin(d* (Math.PI/180)) + trans_y})
  .attr("id",function(d,i){ return i})
  .style("stroke","black")
  .style("stroke-width",function(d){ if (d%45 == 0){ return 2;}
    else {return 1}});

//adding text
var angles_txt = svg.selectAll("text")
  .data(angle)
  .enter()
  .append("text")
  .attr("text-anchor","middle")
  .attr("x",function(d){ if (d%45 == 0){ return (r1+15)*Math.cos(d* (Math.PI/180)) + trans_x;}
    else{  return (r2)*Math.cos(d* (Math.PI/180)) + trans_x;}})
  .attr("y",function(d){ if (d%45 == 0){ return (r1+15)*Math.sin(d* (Math.PI/180)) + trans_y+5;}
    else {return r2*Math.sin(d* (Math.PI/180)) + trans_y;}})
  .attr("id",function(d){return d;})
  .html(function(d){ if (d%45 == 0){ return (360-(d-90))%360;} else {return ""}})
  .style("font-weight", "bold")
  .style("font-size",function(d){ if (d%45 == 0){ return 16;}
    else {return 12}});

//adding pointers
var gpointers = svg.selectAll(".container")
  .data(pointer_data)
  .enter()
  .append("g")
  .classed("container", true)
  .attr("transform", "translate(150, 150)")
  .append("path")
  .attr("id",function(d){return d.id})
  .attr("d",function(d){return d.d})
  .attr("style",function(d){return d.style})
  .style("fill", "Black")
  .style("stroke","#B90925");

//pointer rotation
function rotateElement(originX,originY,towardsX,towardsY){
  if (in_transition==0)
  {
    var degrees = Math.atan2(towardsY-originY,towardsX-originX)*180/Math.PI -90;
    degrees = Math.round(degrees/step)*step     //quantization
    
    if (degrees<=0)
    {
      curr_angle = Math.abs(degrees);
    }
    else
    {
      curr_angle = 360-degrees;
    }

    gpointers.filter("#pointer_ghost").attr("transform",function(d,i){
      d.a =  curr_angle;
      return "rotate("+ (360 - d.a ) +")";      
    });

    /* I think this is IE specific code.  However, my XP version of IE doesn't
     * work so it's bad, bad bad.
     */
    if (window.event.type == "click")
    {
      gpointers.filter("#pointer")
        .transition()
        .each("end",function(){in_transition=0;})
        .each("start",function(){in_transition=1;})
        .duration(function(d){
          var delta = Math.abs(d.a - curr_angle);
          if (delta<=180)
          {
            trans_duration = 1000*message.speed*delta/step;
          }
          else
          {
            trans_duration = 1000*message.speed*(360 - delta)/step;
          }
          return trans_duration;
        })
        .ease("linear")
        .attr("transform",function(d) {
          d.a =  curr_angle;
          message.angle = curr_angle;
          ws.send(message.angle.toString() + ";" + message.speed.toString());
          return "rotate("+ (360 - d.a ) +")";
        });
      }
    }
}


});



</script>   
</body>
</html>
