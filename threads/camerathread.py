#!/usr/bin/python
import threading
import time
import os
from picamera import PiCamera
from time import sleep
from sys import version_info

py3 = version_info[0] > 2

speed = -1
exitFlag = 0

tmpPicName='/var/www/html/campicTmp.jpg'
PicName='/var/www/html/campic.jpg'
camera = PiCamera()

def take_pics:
    #camera.start_preview()
    print ("Writing a new image")
    camera.capture('/var/www/html/campic.jpg')
    while (True):
        sleep(30)
        print ("Writing a new image")
        #
        # This helps avoid stepping on a half loaded file.
        # Of course the problem is going to be deferred so that the file will now be
        # in the process of being served before it gets stepped on.  Will need to
        # figure out some scheme to coordinate that...
        camera.capture(tmpPicName)
        os.rename(tmpPicName,PicName)

    #camera.stop_preview()

def get_motor_params ():
    global speed
    global exitFlag

    getch = _Getch()

    print "Q quit, F faster, S slower"
    response = getch().upper()
    print "Got " + response
    while response != "Q":
        if response == "F":
            print "bumping speed"
            speed = speed + 1
        if response == "S":
            if speed > 0:
                speed = speed - 1
        print "Q quit, F faster, S slower"
        response = getch().upper()
        print "loopGot " + response
    print "gmpSpeed: " + str(speed)
    exitFlag = 1


threads = []
t = threading.Thread(target=take_pics)
threads.append(t)
t.start()

while exitFlag == 0:
    print "wSpeed: " + str(speed) + "<<<"
    sleep(1)

