#!/usr/bin/env python

import threading
import time

exitFlag = 0
speed = 0

class MotorControl(threading.Thread):
    def __init__(self, threadId, name, startSpeed):
        threading.Thread.__init__(self)
        self.threadId = threadId
        self.name = name
        self.startSpeed = startSpeed
        print "MotorControl thread, __init__"

    def run(self):
        print "running MotorControl" + self.name
        old_speed = speed
        while True:
            if exitFlag:
                print "Exiting thread " + self.name
                threadName.exit()
            if old_speed != speed:
                print "setting motor speed to " + str(speed)
                old_speed = speed

class Controller(threading.Thread):
    def __init__(self, threadId, name):
        threading.Thread.__init__(self)
        self.threadId = threadId
        self.name = name
        print "Controller thread, __init__"

    def run(self):
        print "Controller.run"
        while True:
            read_speed = raw_input("Input a speed, -1 to exit:")
            if read_speed < 0:
                exitFlag = 1
                threadName.exit()
            speed = int(read_speed) 




thread1 = MotorControl(1, "thread 1", 1024)
thread2 = Controller(1, "thread 2")

thread1.start()
thread2.start()
