#!/usr/bin/python
import threading
import time
from getch import _Getch
from sys import version_info
from time import sleep

py3 = version_info[0] > 2

speed = -1
exitFlag = 0

def get_motor_params ():
    global speed
    global exitFlag

    getch = _Getch()

    print "Q quit, F faster, S slower"
    response = getch().upper()
    print "Got " + response
    while response != "Q":
        if response == "F":
            print "bumping speed"
            speed = speed + 1
        if response == "S":
            if speed > 0:
                speed = speed - 1
        print "Q quit, F faster, S slower"
        response = getch().upper()
        print "loopGot " + response
    print "gmpSpeed: " + str(speed)
    exitFlag = 1


threads = []
t = threading.Thread(target=get_motor_params)
threads.append(t)
t.start()

while exitFlag == 0:
    print "wSpeed: " + str(speed) + "<<<"
    sleep(1)

