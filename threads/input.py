#!/usr/bin/env python
# Now multithreaded!
import threading
import time
import sys
from sys import version_info
from time import sleep

speed = 0

py3 = version_info[0] > 2

def get_response():
    if py3:
        response = input("Q quit, F faster, S slower")
    else:
        response = raw_input("Q quit, F faster, S slower")

    print "Response: " + response

    while response.upper() != 'Q':
        if response.upper() == 'F':
            speed = speed + 1
        if response.upper() == 'S':
            if speed > 0:
                speed = speed - 1
        if py3:
            response = input("Q quit, F faster, S slower")
        else:
            response = raw_input("Q quit, F faster, S slower")

        print "Response: " + response
        print "Speed: " + str(speed)

    speed = -1

def report_speed():
    old_speed = -3
    while speed > 0:
        if speed != old_speed:
            print speed
            old_speed = speed


try: 
    thread.start_new_thread(get_response(), ())
    thread.start_new_thread(report_speed(), ())
except:
    print "error: unable to start thread"


while 1:
    pass
