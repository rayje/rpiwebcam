import wiringpi
import signal
import sys
import logging
from time import sleep

#
# Pin modes.
#
#
INPUT  = 0
OUTPUT = 1
PWM    = 2

HIGH = 1
LOW = 0

#
# Pull up/down modes
#
PUD_DIS  = 0
PUD_DOWN = 1
PUD_UP   = 2

#
# Directions
# 
FWD = 0
REV = 1

#
# Working on a sort of PWM for each tiny step
# 4 is the minimum effective time, at least at 13v.
# 4 ms for no load.  Bumping up to higher number...
DWELL_TIME = 30  # milliseconds


#
# Drives the stepper motor.  The Raspberry Pi pins connected to the motor driver
# chip are given as initialization parameters.  These are wiring pi pin numbers.
# Maybe later we'll include a mapping function.
# 
# My understanding of the way unipolar steppers work in halfstep mode is that
# only one half of the winding is energized at a time.  What I need to figure
# out now is how to arrange the various combinations to get smooth forward and
# reverse operation.  
# Current arrangements and their results:
#   Works, turns "forward", vibrates a bit
#  1 1001
#  2 1010
#  3 0110
#  4 0101
#
#  To try:
#  1324
#  1342
#  1423
#  1432
#
class StepperDriver:
    def __init__(self, OneA, OneB, TwoA, TwoB, step_state=0):
        self.motorPins = [ OneA, OneB, TwoA, TwoB ]
# Original, mostly working
        self.step1 = [ HIGH, LOW, LOW, HIGH ] # 1001
        self.step1a = [ HIGH, LOW, LOW, LOW ] # 1000
        self.step2 = [ HIGH, LOW, HIGH, LOW ] # 1010
        self.step2a = [ LOW, LOW, HIGH, LOW ] # 0010
        self.step3 = [ LOW, HIGH, HIGH, LOW ] # 0110
        self.step3a = [ LOW, HIGH, LOW, LOW ] # 0100
        self.step4 = [ LOW, HIGH, LOW, HIGH ] # 0101
        self.step4a = [ LOW, LOW, LOW, HIGH ] # 0001

        self.steps_off = [ LOW, LOW, LOW, LOW ] # 0000

        self.steps_fwd = [ self.step4a, self.step4,
                           self.step3a, self.step3,
                           self.step2a, self.step2,
                           self.step1a, self.step1 ] 
        self.steps_rev = [ self.step1, self.step1a,
                           self.step2, self.step2a,
                           self.step3, self.step3a,
                           self.step4, self.step4a ]
        self.direction = FWD

        # Last stepped state. Values can be 
        # 0-3, whichever step was last done.  This is for doing a small step and
        # saving the user from this overhead tracking
        #
        self.stepstate = int(step_state) % len(self.steps_fwd)
        logging.debug("Starting, step state: %s", self.stepstate)
        logging.debug("Len steps: %s", len(self.steps_fwd))

        signal.signal(signal.SIGINT, self.signal_handler)

        ##
        # Finally, initialize wiring pi.
        # This puts the pin numbering scheme into wiring pi mode.  The other
        # modes are sys and gpio.
        # See https://projects.drogon.net/raspberry-pi/wiringpi/functions/
        ##
        wiringpi.wiringPiSetup()

    #
    # At the end of the run, print out the step state.  This is so we can start
    # at the correct state and not jerk the stepper by starting at position 0 
    # Wed  5 Apr 19:05:55 MDT 2017 -- not sure now.  I guess so we could use a
    # shell script?  
    def __del__(self):
        self.cleanup()
        print self.stepstate
#
    #
    # We have to calculate the new step state index.  This is because stepping
    # forward goes from step 4a, 4, 3a, 3,...1 while stepping in reverse we 
    # step in the order of 1, 1a, 2, 2a...4a.  If our stoppping stepstate is
    # say, 1, we are at step4 in the forward direction, but in reverse we are at
    # step1a.  This causes a jump when reversing direction.  To correct this, we
    # have to calculate the matching stepstate between forward and reverse.
    # 
    def go_forward(self):
        if (self.direction != FWD):
            self.stepstate = len(self.steps_fwd) - 1 - self.stepstate
            logging.debug("go_forward, to %s", self.stepstate)
        self.direction = FWD

    def go_reverse(self):
        if (self.direction != REV):
            logging.debug("go_reverse, resetting stepstate from %s", self.stepstate)
            self.stepstate = len(self.steps_rev) - 1 - self.stepstate
            logging.debug("go_forward, to %s", self.stepstate) 
        self.direction = REV

    def signal_handler(self, signal, frame):
        logging.debug('Signal handler called, signal: %s', signal)
        self.cleanup()
        sys.exit(0)


    #
    # Set pin mode to output and enable pull-down resistors.
    # When we set the pin mode to INPUT the pins will float up to 1, triggering
    # the controller, which will burn out the motor.
    #
    def motor_enable(self):
        for pin in self.motorPins:
            wiringpi.pinMode(pin, OUTPUT)
            wiringpi.pullUpDnControl(pin, PUD_DOWN)

    #
    # Re-enable the pull down resistors.  It's redundant but cheap and good
    # insurance at my current level of knowledge.
    #
    def motor_disable(self):
        for pin in self.motorPins:
            wiringpi.pinMode(pin, INPUT)
            wiringpi.pullUpDnControl(pin, PUD_DOWN)

    def cleanup(self):
        self.motor_disable()
        for pin in self.motorPins:
            wiringpi.digitalWrite(pin, LOW)

    
    def motor_write(self, pinstates):
        for (pin,state) in zip(self.motorPins, pinstates):
            wiringpi.digitalWrite(pin, state)

    #
    # This function gives a pulse to the stepper motor pins
    #

    def tiny_step(self, pulse_len):
        if self.direction == FWD:
            steplist = self.steps_fwd
        else:
            steplist = self.steps_rev

        # Send a small step (energize one coil)
#        print "tiny_step, steplist[" + str(self.stepstate) + "]"
        self.motor_write(steplist[self.stepstate])

        self.stepstate = (self.stepstate + 1) % len(steplist)

        # I originally had a wait for the requested dwell time.  However this
        # resulted in jerky motion as it turned the stepper motor on and off.

    #
    # Now am kind of having a bit of trouble figuring out where to put the
    # parameters.  I want to make sure that the enable/disable "guards" are
    # called at the beginning and end of each motion, but not in between.
    #
    # Deprecated.  This results in a very jerky motion and we don't want to
    # really go slowly anyway.  The plan was originally to go slowly to keep
    # from heating up the components.  However, there are better ways of doing
    # that such as tracking the number of degrees traversed per minute or
    # actually sensing the temperature and shutting down (more complicated so
    # it'll be later). Another idea is to have a MOSFET turn off power to the
    # stepper motor when it's not in motion.  This helps to prevent a situation
    # where something happens such as a crash and the power to the steper motor 
    # is not disabled, leading to burned out components and dead batteries.
    # 
    #def full_step(self, speed):
    #    time = 100 - speed
    #    self.motor_enable()
#   #     wiringpi.delay(100) # Sleep a 10th of a second to "stablize"
    #    print "Delay: " + str(time)
    #    for i in range (0,len(self.steps_fwd)):
    #        self.tiny_step(DWELL_TIME)
    #        wiringpi.delay(time)
    #    print "-----"
    #    print self.stepstate
    #    self.motor_disable()

    #
    # Problem: If we move by tiny steps, we need to track the state.  The
    # problem is that we re re-instantiate the object each time so it could
    # start out in the same state.  For example, if it's in state 0, the
    # default, and we want to move 12 degrees, then starting at state 0 again
    # results in a movement of 0 degrees.  Seems bad to have to write and read a
    # file.  Will have to ponder this.
    # NOTE: If this is running in a server, then it might be sufficient to track
    # something like the very first movement encountered and if it's only a
    # single small step then perform two small steps.
    #
    # Later we'll attach a rotary encoder to the stepper back end and monitor
    # the actual movement of the motor.  This is later as it'll be another layer
    # of complexity.
    #
    def rotate(self, degrees, speed):
        time = 100 - speed

        # Calculate num_steps here so that we could possibly add half stepping
        num_steps  = len(self.steps_fwd)

        # 48 tiny_steps per rotation though.
        # 97 tiny_steps (half-steps) per rotation though. (determined
        # empirically)
#        print "steps = " + str(abs(((96.0/360.0)*degrees) * 5))
        steps = abs(int((96.0/360.0)*degrees) * 5)  # 5:1 belt reduction..

        if degrees >= 0: 
            self.go_forward()
        else:
            self.go_reverse()

        #  Break up rotation into N full steps plus remainder tiny steps
        #
        full_steps = steps/num_steps
        partial_steps = steps % num_steps
        logging.debug("rotate: steps: %s", steps)
        logging.debug("rotate: delay time: %s", time)
        logging.debug("rotate: before stepstate: %s", self.stepstate)

        self.motor_enable()
        for i in range (0,steps):
            self.tiny_step(DWELL_TIME)
            wiringpi.delay(time)
        self.motor_disable()

        logging.debug("rotate: after stepstate: %s", self.stepstate)



