import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    '''Create a database connection to SQLite db.
    If it doesn't exist, create it.
    '''
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None

def create_table(conn, create_table_sql):
    '''Crete a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: incoming create table statement
    :return: None
    '''
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def close_connection(conn):
    conn.close()

def add_data(conn, insert_vals):
    """
    Add data into the table
    :param conn:  Connection object
    :param insert_vals: tuple of values to insert
    """
    sql = """INSERT INTO camera_data(cam_attr,cam_val)
             VALUES(?,?)"""
    cur = conn.cursor()
    try:
        cur.execute(sql, insert_vals)
    except Error as e:
        print(e)

    conn.commit()
    return cur.lastrowid


if __name__ == '__main__':
    database = "./camera_data.db"
    sql_create_camera_table = """CREATE TABLE IF NOT EXISTS camera_data (
                                    cam_attr text PRIMARY KEY NOT NULL,
                                    cam_val integer NOT NULL
                                );"""

    conn = create_connection(database)
    if conn is not None:
        create_table(conn, sql_create_camera_table)
    else:
        print("Error: cannot create the database connection!")

    xres = ('xres', 1024)
    yres = ('yres', 768)
    print("xres: id" + str(add_data(conn, xres)))
    print("yres: id" + str(add_data(conn, yres)))

    curpos = ('curpos', 45)
    add_data(conn, curpos)
    close_connection(conn)
