import wiringpi
import sys
import logging

MOVE_WAIT = 1000 # 1000 milliseconds
logging.basicConfig(format='%(asctime)s %(message)s', 
                    datefmt='%m/%d/%Y %I:%M:%S %p', 
                    level=logging.DEBUG,
                    filename="tornado.log")
logging.info("Inspecting...")
logging.debug(wiringpi.__file__)

#
# Hardware constants
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

PWM_MODE_MS = 0

HIGH = 1
LOW = 0

#
# Pull up/down modes
#
PUD_DIS  = 0
PUD_DOWN = 1
PUD_UP   = 2


#
# Fix: Hardcoded...
#
ENABLE_PIN=3

class Servo():

    def __init__(self, pin):
        self.pin = pin
        self.min = 30
        self.max = 95
        self.curpos = 0
        self.degree_range = 140
        ##
        # zero/level out the servo when starting
        ##
        self.mid = (self.min + self.max) / 2
        self.move (self.mid)

    def power_enable(self, pin):
        wiringpi.pinMode(pin, OUTPUT)
        wiringpi.pullUpDnControl(pin, PUD_UP)
        wiringpi.digitalWrite(pin, HIGH)

    def power_disable(self, pin):
        wiringpi.digitalWrite(pin, LOW)
        wiringpi.pinMode(pin, INPUT)
        wiringpi.pullUpDnControl(pin, PUD_DOWN)

    ##
    # Initialize the pin to PWM
    # If we did the initialization, the pwm would continue until shutdown.  
    # My thinking (without oscilloscope or means to measure current in the pi)
    # is to stop the pwm after the move to save electricity.
    ##
    def pre_move(self):
        self.power_enable(ENABLE_PIN)
        wiringpi.pinMode(self.pin, PWM)
        wiringpi.pwmSetMode(PWM_MODE_MS)
        wiringpi.pwmSetClock(400)
        wiringpi.pwmSetRange(1000)

    def post_move(self):
        wiringpi.pinMode(self.pin, OUTPUT)
        wiringpi.digitalWrite(self.pin, LOW)
        self.power_disable(ENABLE_PIN)

    ##
    # Rotate a number of degrees from the midpoint.  Positive degrees point
    # down, negative degrees point up.
    ##
    def rotate_degrees(self, degrees):
        num_per_deg = float((self.max - self.min)) / float(self.degree_range)
        logging.debug("servo.rotate_degrees: num_per_deg: %s", num_per_deg)

        # Add num/deg * degrees because values < mid are negative
        move_num = self.mid + int(num_per_deg * degrees) 
        logging.debug("move_num = self.mid (%s) + int(num_per_deg(%s) * degrees(%s))", self.mid, num_per_deg, degrees)
        logging.debug("servo.rotate_degrees: move_num: %s", move_num)
        
        self.move((self.curpos - self.mid) + move_num)

    ##
    # Move a percent of the full motion
    ## 
    def move_percent (self, pct):
        move_num = int((pct * (self.max - self.min))/100.0) + self.min
        self.move(move_num)

    ##
    # Make the move.  
    # If asked to move outside of min or max, just stop at min or max
    ##
    def move(self, num):
        self.pre_move()
        if (num < self.min):
            num = self.min
            logging.debug("servo.move: move_num (%s) outside of min (%s)", num, self.min)
        if (num > self.max):
            num = self.max
            logging.debug("servo.move: move_num (%s) outside of max (%s)", num, self.max)
        logging.debug("servo.move: moving %s", num)
        self.curpos = num
        wiringpi.pwmWrite(self.pin, num)
        wiringpi.delay(MOVE_WAIT)
        self.post_move()

