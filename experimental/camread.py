from sqlitedict import SqliteDict

def store_val(dict, key, value):
    if key not in dict:
        dict[key] = value

cam_vals = SqliteDict('./camera_data.db', autocommit=True)


for key,value in cam_vals.iteritems():
    print (key,value)

store_val(cam_vals, 'xres', 1024)
store_val(cam_vals, 'yres', 768)
store_val(cam_vals, 'hflip', True)
store_val(cam_vals, 'vflip', True)
    
for key,value in cam_vals.iteritems():
    print (key,value)

cam_vals.close()

