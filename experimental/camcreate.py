from sqlitedict import SqliteDict

cam_vals = SqliteDict('./camera_data.db', autocommit=True)

cam_vals['xres'] = 1024
cam_vals['yres'] = 768

cam_vals['exposure'] = 'night'
cam_vals['curpos'] = 45

for key,value in cam_vals.iteritems():
    print (key,value)

#mydict.close()

