#!/usr/bin/env python
# This tests the concept of turning off power to the servo.  The motor of the
# servo is always energized under normal conditions.  By turning it off, it is
# hoped that energy can be conserved.
#
import time
import wiringpi
import signal
import sys
from steppermotor import StepperDriver

from time import sleep

ENABLE_PIN=3
MIDDLE=65

if (len(sys.argv) > 1):
    POSITION=int(sys.argv[1])
else:
    POSITION=MIDDLE


#
# Pin modes.
# 
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

HIGH = 1
LOW = 0

#
# Pull up/down modes
#
PUD_DIS  = 0
PUD_DOWN = 1
PUD_UP   = 2

PWM_MODE_MS = 0
pin = 1
wiringpi.wiringPiSetup()

def power_enable(pin):
    wiringpi.pinMode(pin, OUTPUT)
    wiringpi.pullUpDnControl(pin, PUD_UP)
    wiringpi.digitalWrite(pin, HIGH)

def power_disable(pin):
    wiringpi.pinMode(pin, INPUT)
    wiringpi.pullUpDnControl(pin, PUD_DOWN)
    wiringpi.digitalWrite(pin, LOW)

def pwm_init(pin):
    power_enable(ENABLE_PIN)
    wiringpi.pinMode(pin, PWM)
    wiringpi.pwmSetMode(PWM_MODE_MS)
    wiringpi.pwmSetClock(400)
    wiringpi.pwmSetRange(1000)

def pwm_shutdown(pin):
    wiringpi.pinMode(pin, OUTPUT)
    wiringpi.digitalWrite(pin, LOW)
    power_disable(ENABLE_PIN)

DELAY=30
pwm_init(pin)

wiringpi.pwmWrite(pin, POSITION)

#wiringpi.pwmWrite(pin, 65)
wiringpi.delay(DELAY * DELAY)

pwm_shutdown(pin)

#sleep(5)
exit()
