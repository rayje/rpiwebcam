import logging
import logging.config

logging.config.fileConfig('logging.conf')
logger = logging.getLogger('simpleExample')


class firstclass():
    def __init__(self):
        logging.info("firstclass.__init__, info message")
        logger.info("gottenfirstclass.__init__, info message")

    def do_something(self):
        logger.info("do_something, info message")


    def doitagain(self):
        logger.debug("start of doitagain")
        self.do_something()
        logger.debug("end of doitagain")
        

class secondclass():
    def __init__(self):
        logger.info("secondclass.__init__, info message")



obj1 = firstclass()
logger.info("(logger)after creating obj1")
logging.info("(logging)after creating obj1")

obj2 = secondclass()
logger.info("(logger)after creating obj2")
logging.info("(logging)after creating obj2")

obj1.do_something()
