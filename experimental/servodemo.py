#!/usr/bin/env python
import time
import wiringpi
import signal
import sys
from steppermotor import StepperDriver

from time import sleep



#
# Pin modes.
# 
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

HIGH = 1
LOW = 0

#
# Pull up/down modes
#
PUD_DIS  = 0
PUD_DOWN = 1
PUD_UP   = 2

PWM_MODE_MS = 0
pin = 1
wiringpi.wiringPiSetup()

def pwm_init(pin):
    wiringpi.pinMode(pin, PWM)
    wiringpi.pwmSetMode(PWM_MODE_MS)
    wiringpi.pwmSetClock(400)
    wiringpi.pwmSetRange(1000)

def pwm_shutdown(pin):
    wiringpi.pinMode(pin, OUTPUT)
    wiringpi.digitalWrite(pin, LOW)
    wiringpi.pinMode(pin, INPUT)

DELAY=30
pwm_init(pin)
for i in range (95, 30, -1):
    wiringpi.pwmWrite(pin, i)
    wiringpi.delay(DELAY)

wiringpi.pwmWrite(pin, 65)
wiringpi.delay(DELAY * DELAY)

pwm_shutdown(pin)

#sleep(5)
exit()
