#Borg Pattern from 
# http://stackoverflow.com/questions/1318406/why-is-the-borg-pattern-better-than-the-singleton-pattern-in-python
CLOSED=0
OPEN=1
class Borg:

    __monostate = None

    def __init__(self):
        if not Borg.__monostate:
            Borg.__monostate = self.__dict__
            #Your definitions here
            self.x = 1
            self.shutter = 'closed'

        else:
            self.__dict__ = Borg.__monostate




if __name__ == "__main__":
    print "State check:"
    A = Borg()
    B = Borg()

    print "At first: B.x: = {} and A.x = {}".format(B.x, A.x)
    print "At first: B.shutter: = {} and A.shutter = {}".format(B.shutter, A.shutter)
    A.x = 2
    print "After A.x = 2"
    print "Now both first: B.x: = {} and A.x = {}".format(B.x, A.x)
    print  "Are A and B the same object? Answer: {}".format(id(A)==id(B))

    A.shutter='open'
    print "after A.shutter='open'"
    print "Now both first: B.shutter: = {} and A.shutter = {}".format(B.shutter, A.shutter)
