#!/usr/bin/env python
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import time
import wiringpi
import signal
import sys
import logging
import socket
import urlparse
from camera import Camera
from steppermotor import StepperDriver
from irfilter import FilterDriver
from tornado import gen, ioloop, queues
from concurrent.futures import ThreadPoolExecutor
from servo import Servo


from time import sleep

from tornado.util import PY3
if PY3:
    from urllib.parse import urlparse  # py2

    xrange = range
else:
    from urlparse import urlparse  # py3

clients = []

LISTEN_PORT=8888
PIC_SNAP_SIM_TIME = 6    # How long a simulated picture snap takes.
PIC_SNAP_DELAY=30        # Normal pace of updates
PIC_MIN_SNAP_PERIOD = 20 # No pictures less than this many seconds apart

logging.basicConfig(format='%(asctime)s %(message)s', 
                    datefmt='%m/%d/%Y %I:%M:%S %p', 
                    level=logging.DEBUG,
                    filename="tornado.log")
logging.info("Inspecting...")
logging.debug(wiringpi.__file__)
logging.debug("done inspecting")
logging.info("Connect to the web socket to start the machinery")

#
# Pin modes.
# 
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

HIGH = 1
LOW = 0

MOV_SPEED = 70
MAX_ROTATE= 55

SERVO_PIN = 1

# This should probably go in the module as it's not going to change much at
# all, but we'll leave it here for now.
Motor1A = 21  # maps to physical pin 29/pi wedge g5
Motor1B = 22  # maps to physical pin 31/pi wedge g6
Motor2A = 23  # maps to physical pin 33/pi wedge g13
Motor2B = 25  # maps to physical pin 37/pi wedge g26

##
# The mapping of sparkfun piwedge pin names to the output of gpio readall is found
# on the BCM column.  The "Name" column from gpio is the wiring pi name, not the name
# found on the piwedge.  The BCM column corresponds with the "G name" silkscreened on the
# piwedge.
##
FilterA  = 28  # maps to piwedge G20, gpio Name: GPIO.28, phys 38
FilterB  = 24  # maps to piwedge G19, gpio Name: GPIO.24, phys 35
FilterEnable = 0  # maps to piwedge G17, gpio Name: GPIO.0, phys 11



##
#Borg Pattern from
# http://stackoverflow.com/questions/1318406/why-is-the-borg-pattern-better-than-the-singleton-pattern-in-python
##
class CamState:

    __monostate = None

    def __init__(self):
        if not CamState.__monostate:
            CamState.__monostate = self.__dict__
            ##
            # Reasonable initial state values here
            ##
            self.shutter = 'closed'
            self.lastsnap = 0
            

        else:
            self.__dict__ = CamState.__monostate


#
# TODO: Move the command manipulation functions out of this class into their
# own.
class Client():
    def __init__(self):
        self.cam_state = CamState()
        self.camera = Camera('/var/www/html', 'campic.jpg')
        self.servo = Servo(SERVO_PIN)
        self.q = queues.Queue()
        self.camera.set_res(1024, 768)
        self.camera.set_exposure_type('day')
        self.cam_state.exposure = "D"
        self.cam_state.resolution = "Z"
        self.allowed_resolutions = {"3280x2464",
                                    "2592x1944", 
                                    "1920x1080", 
                                    "1640x1232",
                                    "1280x720",
                                    "1024x768", 
                                    "640x480", 
                                    "320x240"}
        # Map of the commands, their possible values and numerical mappings 
        self.parsemap = {'E':{'D': 1, 'E':2, 'N':3}, 'F':{'IN': 1, 'OUT':2}}
        self.parsefuncs = {
                         'E':self.cons_exp, 
                         'F':self.cons_filt,
                         'X':self.avg_coords,
                         'Y':self.avg_coords,
                         'C':self.all2first,
                         'S':self.all2first,
                         'M':self.all2first,
                         'Z':self.all2last
                        }
        self.servo = Servo(SERVO_PIN)

    ##
    # Going to try queue retrieval here and log out a message of things
    # retrieved.  Will put in a 30 second sleep later
    #
    # Commands:
    #   C: Shutter closed 
    #   E: Set exposure
    #   F: IR filter in or out
    #   M: Send a message
    #   Q: Set frequency of picture taking
    #   S: Snap picture
    #   X: Move in X direction
    #   Y: Move in Y direction
    #   Z: Set size (resolution) of image
    # TODO:  Save the commands in a dictionary of lists.  This will make
    # consolidation and ordered extraction easier.
    ##
    @gen.coroutine
    def process_queue(self):
        cmd_dict = {}
        while True:
            (command, value) = yield self.q.get()
            logging.info("process_queue: retrieved cmd: \"%s\" value: \"%s\"", command, value)

            ##
            # Push the command on the stored commands.  
            # this point we aren't too concerned about performance since it
            # shouldn't be handling tons of commands to begin with.
            ##
            self.store_command(cmd_dict, command, value)

            ##
            # Store commands if the shutter is open.
            # If closed, check to see if the stored commands list is non-empty.
            # If so, process the backed-up commands first, then process the new
            # command.  
            # A shutter closed command will come through when the camera
            # finishes snapping a picture, so we aren't stuck waiting for the
            # next command to come through before processing the stored
            # commands.
            # We must do this as FIFO (hence the pop(0)) because a snap command
            # may come in while processing.
            ##
            if (self.cam_state.shutter != 'open'):
                logging.debug ("process_queue: Shutter is not open, processing commands")
                self.merge_stored_vals(cmd_dict)
                for command in cmd_dict:
                    logging.debug ("process_queue: processing stored command:(%s, %s)", command, cmd_dict[command][0])
                    try:
                        self.process_command (command, cmd_dict[command][0])
                    except Exception as e:
                        logging.error("ERROR: process_queue exception: command: %s, error: %s",\
                                      command, e)
                    cmd_dict[command] = []
                cmd_dict = {}
            else:
                logging.debug ("process_queue: Shutter is open, not processing")

    ##
    # Add a command to the dictionary of lists that stores the commands until
    # they can be processed.
    # The question right now is whether or not to make this specific or generic.
    # Generic is easier to maintain, specific is faster.  With 30 sec. between
    # shots, generic it is.
    ##
    def store_command(self, cmd_dict, command, value):
        command = command.upper()
        logging.info ("store_command: command: %s, value: %s", command, value)
        if command not in cmd_dict:
            logging.debug ("store_command: %s not in cmd_dict, adding", command)
            cmd_dict[command] = []

        cmd_dict[command].append(value)
        logging.debug ("store_command, appended %s to cmd_dict[%s]: %s", \
                       value, command, cmd_dict[command])


    def state_string(self):
        state = CamState()
        seq = (
            'C:{}'.format(len(clients)),
            'F:{}'.format(state.IRFilter),
            'E:{}'.format(state.exposure),
            'Z:{}'.format(state.resolution)
            )
        return ';'.join(seq)

    ##
    # consolidate exposures.  Going to try taking the average for now
    ##
    def cons_exp(self, vals):
        total = 0.0
        count = 0
        valmap = ['D', 'E', 'N']
        for val in vals:
            if val == 'D':
                # total += 0.0 
                count += 1
            elif val == 'E':
                total += 1.0
                count += 1
            elif val == 'N':
                total += 2.0
                count += 1

        vals[0] = valmap[int(round(total/count))]
        del vals[1:]
    ##
    # consolidate filter settings.  Going to try taking the average for now
    ##
    def cons_filt(self, vals):
        total = 0.0
        count = 0
        valmap = ['IN', 'OUT']
        for val in vals:
            if val == 'IN':
                # total += 0.0 NOOP
                count += 1
            elif val == 'OUT':
                total += 1.0
                count += 1

        vals[0] = valmap[int(round(total/count))]
        del vals[1:]


    ##
    # consolidate coordinate based settings. Average again
    ##
    def avg_coords(self, vals):
        total = 0.0
        for val in vals:
                total += float(val)

        vals[0] = total/len(vals)
        del vals[1:]

    ##
    # collapse all values into the last one
    ##
    def all2last(self, vals):
        del vals[:-1]
        
    ##
    # collapse all values into the first one
    ##
    def all2first(self, vals):
        del vals[1:]

    ##
    # Iterate over the list in the stored values list.
    # Each command type will have its own merging function, stored in parsefuncs 
    # E.g. directional commands will be averaged, Snap commands will just map to
    # 1, etc.
    ##
    def merge_stored_vals(self, cmd_dict):
        logging.info("merge_stored_vals: cmd_dict: %s", cmd_dict)
        for cmd in cmd_dict:
            logging.debug("merge_stored_vals: cmd: %s", cmd)
            if cmd in self.parsefuncs:
                self.parsefuncs[cmd](cmd_dict[cmd])
            else:
                logging.warning("merge_stored_vals: received unexpected command: %s", cmd)
            logging.debug("merge_stored_vals result: %s", cmd_dict[cmd])
                

    ##
    # Make the calls for each command received.
    # TODO: Put each command handler into its own function.
    # Create a dictionary with the commands in it with the functions as value.
    ##
    def process_command(self, command, value):
        cam = SnapPicture()
        self.IRfilter = self.cam_state.IRfilterDriver
        call_id = 0
        logging.info("process_command: call ID: %s , command: %s, value: %s",\
            call_id, command, value)

        ##
        # The returned amount is now 
        if (command.upper() == "X"): # Pan a percent.  
            logging.debug("X command handler: rotating %s percent",value)
            try:
                percent = float(value)
            except:
                logging.warning ("process_command: Exception: Unexpected string value: %s", value)
                return
            if (percent > 100 or percent < -100):
                logging.warn('Warning: Supplied percentage out of range, ignoring')
                return
            ##
            # Calculate degrees: 
            # percent ranges from -100 to +100
            # Take half the field of view because we are working out from the
            # middle or half the field of view
            ##
            xFov = self.camera.getXfov("degrees")
            idegrees = int(xFov/2.0 * (percent/100.0))

            logging.debug("process_command: moving by %s degres", idegrees)
            self.cam_state.stepperDriver.motor_enable()
            self.cam_state.stepperDriver.rotate(idegrees, MOV_SPEED)
            self.cam_state.stepperDriver.cleanup()
            logging.debug("  move done")
        elif (command.upper() == "Y"): # Tilt Y degrees
            ##
            # TODO: Need to implement a tilt lock.  Without it, all clicks will
            # result in wiggling up and down all the time.  This seems like it's
            # not what you'd want to see.
            ##
            logging.debug("Y command handler: Tilting %s percent",value)
            try:
                percent = float(value)
            except:
                logging.warning ("process_command: Exception: Unexpected string value: %s", value)
                return
            if (percent > 100 or percent < -100):
                logging.warn('Warning: Supplied percentage out of range, ignoring')
                return
            ##
            # Calculate degrees: 
            # percent ranges from -100 to +100 
            # NOTE: We can do this but since the servo only moves up and down a
            # fixed amount, we can't just kepe throwing numbers at it.
            # Eventually it'll hit the end of its travel.  This is handled by
            # the servo itself, but eventually we'll hit the end of servo travel
            # as we click around.
            ##
            yFov = self.camera.getYfov("degrees")
            logging.debug("yFov: %s degrees", yFov)
            idegrees = int(yFov/2.0 * (percent/100.0))
            logging.debug("idegrees: %s degrees", idegrees)


            logging.debug("Tilting by %s degrees", idegrees)
            self.servo.rotate_degrees(idegrees)
            logging.debug("  tilt done")

        elif (command.upper() == "F"): # IR Filter
            logging.info("IRfilter %s",value)
            if (value.upper() == "IN"):
                logging.info("putting filter in")
                self.IRfilter.filterIn()
                self.cam_state.IRFilter = "IN"
            else:
                logging.info("taking filter out")
                self.IRfilter.filterOut()
                self.cam_state.IRFilter = "OUT"
        
        elif (command.upper() == "S"):  # Snap
            logging.debug("Snap command received")
            cam.take_pic(call_id, self.camera)
            logging.debug("Snap command complete")

        elif (command.upper() == "E"):  # Exposure
            self.set_exposure(value)
            logging.debug("Exposure command received: %s", value)
            

        elif (command.upper() == "Z"):  # Size
            self.set_resolution(value)
            logging.debug("Size command received: %s", value)

        elif (command.upper() == "Q"):  # Frequency
            logging.debug("Frequency command received: %s", value)

        elif (command.upper() == "M"):  # Message
            logging.debug("Message: %s", value)
        logging.debug("process_command: returned from take_pic; Count: %s", call_id)
        call_id += 1

        ##
        # Send the state info to all the connected clients so their controls
        # reflect the actual state of the camera.
        ##
        state = self.state_string()
        logging.info('process_command: sending %s to %s clients', state, len(clients))
        for client in clients:
            client.write_message(state)


    ##
    # Set the camera's resolution.  
    ##
    def set_resolution(self, value):
        state = CamState()
        logging.info("set_resolution, value: %s", value)

        if (value in self.allowed_resolutions):
            (x,y) = value.split('x')
        else:
            (x,y) = (1024,768) # Safe default

        logging.debug("setting resolution to (%s, %s)", x, y)
        state.resolution = x + "," + y
        self.camera.set_res(int(x),int(y))

            
            
    ##
    # Set the camera's exposure and any accompanying values
    # Exposure states are
    # N: Night
    # E: Dusk/evening
    # D: Day
    ##
    def set_exposure(self, value):
        logging.info("set_exposure, value: %s", value)
        state = CamState()
        if (value == 'N'):
            self.camera.set_exposure_type('night')
            state.exposure = 'N'
        elif (value == 'E'):
            self.camera.set_exposure_type('dusk')
            state.exposure = 'E'
        elif (value == 'D'):
            self.camera.set_exposure_type('day')
            state.exposure = 'D'

    ##
    # Stuffing this here.  Seems like I'm stabbing in the dark...
    ##
    @gen.coroutine
    def send_snap(self):
        logging.info("client.send_snap, enqueuing snap command")
        yield client.q.put(('S', 1))
        logging.debug("client.send_snap, send_snap done")


class SnapPicture():
    @gen.coroutine
    def take_pic(self, call_id, picam):
        state = CamState()

        logging.info("SnapPicture(%s):take_pic starting", call_id)

        if state.shutter == 'closed':
            logging.info("SnapPicture(%s), shutter is closed", call_id)
            now = time.time()
            logging.info("SnapPicture(%s), long enough between shots? Answer: %s", 
                            call_id, 
                            (now - state.lastsnap) > PIC_MIN_SNAP_PERIOD)
            ##
            # Use this to throttle snapping.  
            # Eventually we'll snap a picture right after a move, but
            # we're not going to allow a miniscule move to defeat the
            # periodicity of the snapping.
            # Publish a shutter closed event to trigger processing of any backed
            # up events.
            ##
            if (now - state.lastsnap) > PIC_MIN_SNAP_PERIOD: 
                logging.debug("SnapPicture.take_pic(%s), updating lastsnap to %s", call_id, now)
                state.lastsnap = now
                state.shutter = 'open'
                logging.info("SnapPicture.take_pic(%s), taking picture", call_id)
                yield executor.submit(picam.snap)
                logging.debug("SnapPicture.take_pic(%s), picture done", call_id)
                state.shutter = 'closed'

                logging.debug("In SnapPicture.take_pic, enqueuing (C,1)")
                yield client.q.put(('C', 1))
                logging.debug("SnapPicture.take_pic: close enqueued")
            else:
                logging.debug("SnapPicture:take_pic(%s), not long enough between pictures: %s - %s = %s",
                        call_id, now, state.lastsnap, now - state.lastsnap)
        else:
            logging.debug("SnapPicture(%s) skipping because shutter is %s", call_id,state.shutter)

        logging.debug("SnapPicture(%s), done", call_id) 


class WSHandler(tornado.websocket.WebSocketHandler):

    def __init__(self, application, request, **kwargs):
        super(WSHandler, self).__init__(application, request, **kwargs)
        logging.info("in WSHandler.__init__")

    def initialize(self, motordriver, filterdriver):
        self.stepper = motordriver
        self.IRfilter = filterdriver

    ##
    # From
    # http://stackoverflow.com/questions/24800436/under-tornado-v4-websocket-connections-get-refused-with-403
    ##
    def check_origin(self, origin):
        """Override to enable support for allowing alternate origins.

        The ``origin`` argument is the value of the ``Origin`` HTTP header,
        the url responsible for initiating this request.

        .. versionadded:: 4.0
        """
        parsed_origin = urlparse(origin)
        origin = parsed_origin.netloc
        origin = origin.lower()
        logging.debug("check_origin: unresolved origin is %s", origin)

        ##
        # Discard the port and look up both host and origin.
        ##
        host = self.request.headers.get("Host").split(':')[0]
        logging.debug("check_origin: unresolved host is %s", host)
        try:
            hostip = socket.gethostbyname(host)
        except Exception as e:
            logging.error("check_origin exception: %s, hostname: %s", e, host)
            
        host = hostip

        try:
            originip = socket.gethostbyname(origin)
        except Exception as e:
            logging.error("check_origin exception: %s, origin: %s", e, origin)
            
        origin = originip

        logging.debug("check_origin: Origin is %s", origin)
        logging.debug("check_origin: Host is %s", host)



        ##
        # Allow local connections 
        ##
        if (origin == "192.168.1.10"):
            logging.debug("check_origin: Host is 192.168.1.10, returning true")
            return True

        if (origin != host):
            logging.warning("Not allowing: origin: %s, host: %s", origin, host)
        ##
        # Check to see that origin matches host directly.  
        ##
        return origin == host

    def state_string(self):
        state = CamState()
        seq = (
            'C:{}'.format(len(clients)),
            'F:{}'.format(state.IRFilter),
            'E:{}'.format(state.exposure),
            'Z:{}'.format(state.resolution)
            )
        return ';'.join(seq)

    ##
    # Push the current set of values to the web browser
    ##
    def open(self):
        clients.append(self)
        logging.info("Open: Connected")
        logging.info("Sending states")
        self.write_message(self.state_string())
        logging.info("Open, sending message to connecting client: %s", self.state_string())
        
    @gen.coroutine
    def enqueue(self, type, strval):
        logging.debug("----------------")
        logging.debug("In enqueue, queuing (%s,%s)", type, strval)
        yield client.q.put((type, strval))
        logging.debug("enqueue done")


    def on_message(self, message):
        logging.info('on_message: received message: %s', message)
        data = message.split(";")
        ##
        # Basic data type and format checking.  Range checking done by
        # individual handlers.
        ##
        for item in data:
            try:
                (type,strval) = item.split(":")
            except ValueError:
                logging.info("Unparseable message: %s", message)
                logging.info("Syntax: X:xval[;Y:yval[;M:message]]")
                return

            self.enqueue(type, strval)

    def on_close(self):
        logging.debug("on_close:connection closed")
        clients.remove(self)


if __name__ == "__main__":
    cameraState = CamState()
    stepperDriver = StepperDriver(Motor1A, Motor1B, Motor2A, Motor2B)
    IRfilterDriver = FilterDriver(FilterA, FilterB, FilterEnable)
    IRfilterDriver.filterIn()
    cameraState.stepperDriver = stepperDriver
    cameraState.IRfilterDriver = IRfilterDriver
    cameraState.IRFilter = "IN"

    executor = ThreadPoolExecutor(1)
    snapper = ThreadPoolExecutor(1)
    client = Client()

    tornado.ioloop.IOLoop.instance().add_callback(client.process_queue)

    application = tornado.web.Application([
                  (r'/ws', WSHandler, dict(motordriver=stepperDriver,
                  filterdriver=IRfilterDriver)),
                  ])

    tornado.ioloop.PeriodicCallback(client.send_snap, (PIC_SNAP_DELAY * 1000)).start()

    http_server = tornado.httpserver.HTTPServer(application, ssl_options={
                                                "certfile": "/etc/letsencrypt/live/chipstruck.com/cert.pem",
                                                "keyfile":"/etc/letsencrypt/live/chipstruck.com/privkey.pem"}
                                               )
    http_server.listen(LISTEN_PORT)
    main_loop = tornado.ioloop.IOLoop.instance()
    main_loop.start()

