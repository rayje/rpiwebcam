#!/usr/bin/env python
import wiringpi
import logging
import signal
import sys
from irfilter import FilterDriver
from time import sleep
#
# Purpose: 
#   Test the IR filter in/out capability.
# Introduction:
#   The IR filter is a solenoid controlled device.  Control is performed by
#   reversing the polarity of current to the solenoid.
# Setup:
#   A pair of LEDs in parallel facing opposite directions.  
#   1K resister on one end of the pair
#   One control output to one end of the parallel LEDs
#   The other control output to the other end of the parallel LEDs
# Tests:
#   NOTE: LEDs A and B should be consistent between tests but these tests don't
#         distinguish between "in" and "out" leds
#
#   Flickering filter IN:
#       Should light one of the pair of LEDs, flickering
#       LED A | LED B
#       ------+------
#       ON    | OFF
#
#   Flickering filter OUT
#       Should light the other of the pair of LEDs, flickering
#       LED A | LED B
#       ------+------
#       OFF   | ON
#       (for 1 second)
#
#   Sustained filter IN:
#       Should light one of the pair of LEDs, blinking with longer on period
#       LED A | LED B
#       ------+------
#       ON    | OFF
#
#   Sustained filter OUT
#       Should light the other of the pair of LEDs, blinking with longer on
#       period
#       LED A | LED B
#       ------+------
#       OFF   | ON
#       (for 1 second)
#
#   Power on/off to filter IN
#       Should flicker both of the pair of LEDs
#       LED A | LED B
#       ------+------
#       OFF   | ON
#       OFF   | OFF
#       OFF   | ON
#       OFF   | OFF
#       OFF   | ON
#       OFF   | OFF
#       .... (for 1 second)
#
#   Test End:
#       LEDs alternate with decreasing on time until they both appear on (sort
#       of :-) ).  
#       LED A | LED B
#       ------+------
#       ON    | OFF
#       OFF   | ON
#       ON    | OFF
#       OFF   | ON
#       ON    | OFF
#       OFF   | ON
#           
#



##
# The mapping of sparkfun piwedge pin names to the output of gpio readall is found
# on the BCM column.  The "Name" column from gpio is the wiring pi name, not the name
# found on the piwedge.  The BCM column corresponds with the "G name" silkscreened on the
# piwedge.
##
FilterA  = 28  # maps to piwedge G20, gpio Name: GPIO.28, phys 38
FilterB  = 24  # maps to piwedge G19, gpio Name: GPIO.24, phys 35
FilterEnable = 0  # maps to piwedge G17, gpio Name: GPIO.0, phys 11




IRfilterDriver = FilterDriver(FilterA, FilterB, FilterEnable, setupPi = True)



# Filter In: 
for i in range(1,10):
    IRfilterDriver.filterIn()
    wiringpi.delay(20)

# Filter Out: 
for i in range(1,10):
    IRfilterDriver.filterOut()
    wiringpi.delay(20)

IRfilterDriver.setDelay(100)

for i in range(1,2):
    IRfilterDriver.filterIn()
    wiringpi.delay(20)
    
for i in range(1,2):
    IRfilterDriver.filterOut()
    wiringpi.delay(20)

for i in range(1,10):
    IRfilterDriver.filterIn()
    IRfilterDriver.filterOut()

for delay in range(50, 1, -1):
    IRfilterDriver.setDelay(delay)
    for i in range(1,5):
        IRfilterDriver.filterIn()
        IRfilterDriver.filterOut()
