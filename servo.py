import wiringpi
import sys
import logging
import random
from sqlitedict import SqliteDict

from time import sleep

MOVE_WAIT = 2000 # milliseconds
logging.basicConfig(format='%(asctime)s %(message)s', 
                    datefmt='%m/%d/%Y %I:%M:%S %p', 
                    level=logging.DEBUG,
                    filename="tornado.log")
logging.info("Inspecting...")
logging.debug(wiringpi.__file__)

#
# Hardware constants
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

PWM_MODE_MS = 0

HIGH = 1
LOW = 0

#
# Pull up/down modes
#
PUD_DIS  = 0
PUD_DOWN = 1
PUD_UP   = 2


#
# Fix: Hardcoded...
#
ENABLE_PIN=3

class Servo():

    def __init__(self, dbname, pin):
        self.servo_vals = SqliteDict(dbname, autocommit=True)

        self.pin = pin
        self.min = 30 # Pulse width
        self.max = 95 # Pulse width
        self.degree_range = 150
        ##
        # zero/level out the servo when starting
        ##
        self.mid = (self.min + self.max) / 2
        logging.debug ("servo.__init__, min: %s, max: %s", self.min, self.max)
        logging.debug ("servo.__init__, moving to mid point: %s", self.mid)

        self.save_default(self.servo_vals, 'curpos', self.mid)
        self.move (self.servo_vals['curpos'])

    def save_default(self, nv_dict, key, value):
        '''Store a keyvalue pair into a dictionary or SqliteDict in particular
           :param nv_dict:  dictionary name
           :param key:      key
           :param value:    value
        '''
        if key not in nv_dict:
            nv_dict[key] = value

    def get_min(self):
        return self.min

    def get_max(self):
        return self.max

    def get_mid(self):
        return self.mid

    def power_enable(self, pin):
        wiringpi.pinMode(pin, OUTPUT)
        wiringpi.pullUpDnControl(pin, PUD_UP)
        wiringpi.digitalWrite(pin, HIGH)

    def power_disable(self, pin):
        wiringpi.digitalWrite(pin, LOW)
        wiringpi.pinMode(pin, INPUT)
        wiringpi.pullUpDnControl(pin, PUD_DOWN)


    ##
    # Initialize the pin to PWM
    # If we did the initialization, the pwm would continue until shutdown.  
    # My thinking (without oscilloscope or means to measure current in the pi)
    # is to stop the pwm after the move to save electricity.
    ##
    def pre_move(self):
        logging.debug("servo.pre_move.")
        self.power_enable(ENABLE_PIN)
        wiringpi.pinMode(self.pin, PWM)
        wiringpi.pwmSetMode(PWM_MODE_MS)
        wiringpi.pwmSetClock(400)
        wiringpi.pwmSetRange(1000)
        logging.debug("servo.pre_move.  Done")

    def post_move(self):
        logging.debug("servo.post_move.")
        wiringpi.pinMode(self.pin, OUTPUT)
        wiringpi.digitalWrite(self.pin, LOW)
        self.power_disable(ENABLE_PIN)
        logging.debug("servo.post.  Done")

    ##
    # Rotate a number of degrees from the midpoint.  Positive degrees point
    # down, negative degrees point up.
    ##
    def rotate_degrees(self, degrees):
        pwm_per_deg = float((self.max - self.min)) / float(self.degree_range)
        logging.debug("servo.rotate_degrees: requested degrees: %s", degrees)

        # Add num/deg * degrees because values < mid are negative
        #move_pwm_num = int(self.servo_vals['curpos'] - (pwm_per_deg * degrees))
        move_pwm_num = int(self.servo_vals['curpos'] + (pwm_per_deg * degrees))
        logging.debug("move_pwm_num %s = int(self.servo_vals['curpos'] (%s) - pwm_per_deg(%s) * degrees(%s))", 
                      move_pwm_num, self.servo_vals['curpos'], pwm_per_deg, degrees)
        self.move(move_pwm_num)

    ##
    # Move a percent of the full motion
    ## 
    def move_percent (self, pct):
        move_num = int((pct * (self.max - self.min))/100.0) + self.min
        logging.debug("servo.move_percent: requested percent: %s", percent)
        self.move(move_num)

    ##
    # Make the move in absolute numbers.  
    # If asked to move outside of min or max, just stop at min or max
    ##
    def move(self, num):
#        logging.debug("servo.move, DISABLED. Requested num: %s", num)
#        return

        logging.debug("servo.move, requested num: %s", num)

        if (num <= self.min):
            num = self.min
            logging.debug("servo.move: move_num (%s) less than range (%s)", num, self.min)
        elif (num >= self.max):
            logging.debug("servo.move: move_num (%s) greater than range (%s)", num, self.max)
            num = self.max

        if (num == self.servo_vals['curpos']):
            logging.debug("servo.move: move_num (%s) same as curpos, not moving", num)
            return

        self.pre_move()
        logging.debug("servo.move: moving %s", num)
        wiringpi.pwmWrite(self.pin, num)
        wiringpi.delay(MOVE_WAIT)
        self.servo_vals['curpos'] = num
        self.post_move()

        logging.debug ("servo.move: Complete,curpos: (%s)", self.servo_vals['curpos'])


if __name__ == '__main__':
    SERVO_PIN = 1
    MOVE_WAIT=100
    DELAY_MS=1
    wiringpi.wiringPiSetup()
    servo = Servo(SERVO_PIN)

    RAND_HI=servo.get_max()
    RAND_LO=int(servo.get_max() * .85)

    servo.move(servo.get_min())
    wiringpi.delay(DELAY_MS * 4)
    servo.move(servo.get_max())
    wiringpi.delay(DELAY_MS * 4)

#    for i in range(servo.get_min(), servo.get_max()):
#        print("Moving " + str(i))
#        servo.move(i)
#        wiringpi.delay(DELAY_MS)
#    
#    print("Done with first moves")
#    wiringpi.delay(4 * DELAY_MS)
#
#    for i in range(servo.get_max(), servo.get_min(), -1):
#        print("Moving " + str(i))
#        servo.move(i)
#        wiringpi.delay(DELAY_MS)
#
    print("Returning to middle")
    move_val=servo.get_mid()
    for i in (1,2):
        wiringpi.delay(DELAY_MS)
        servo.move(servo.get_mid())
        print("Now random moves between " + str(RAND_LO) + " and " + str(RAND_HI))
        for i in range(1,100):
            move_val = random.randint(RAND_LO, RAND_HI)
            print("Moving " + str(move_val))
            servo.move(move_val)
            wiringpi.delay(DELAY_MS)

        print("Done with last moves, returning to middle")
        servo.move(servo.get_mid())
        RAND_HI=RAND_LO
        RAND_LO=servo.get_min()


if __name__ == '__main__':
    SERVO_PIN = 1
    self.servo = Servo('./test_servo_data.db', SERVO_PIN)
