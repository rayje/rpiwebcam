import sys
from sqlitedict import SqliteDict

if len(sys.argv) < 2: 
    print("Usage: " + sys.argv[0] + "<dbfile name>")
    sys.exit()

db_vals = SqliteDict(sys.argv[1], autocommit=True)

for key,val in db_vals.iteritems():
    print (key, val)

db_vals.close()
