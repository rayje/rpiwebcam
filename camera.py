import os
import time
import sys
import picamera
import tempfile
import logging
import math
from picamera import PiCamera
from time import sleep
from datetime import datetime
from PIL import Image, ImageDraw
from readtemp import Temperature
from sqlitedict import SqliteDict

from fractions import Fraction

class Camera():

    def __init__(self, dbname, dirname, fname):
        '''Create camera and start with some reasonable defaults
           :param dirname: Output directory for snapshots
           :param fname:   Output file name for snapshots
        '''
        self.camera = PiCamera()
        self.temp = Temperature()
        self.camera_sleep = 0

        # Populate the default values if needed.
        self.cam_vals = SqliteDict(dbname, autocommit=True)

        self.save_default(self.cam_vals, 'hflip', True)
        self.save_default(self.cam_vals, 'vflip', True)
        self.save_default(self.cam_vals, 'exposure_type', 'auto')
        self.save_default(self.cam_vals, 'PicName', os.path.join(dirname, fname))
        self.save_default(self.cam_vals, 'x_res', 1024)
        self.save_default(self.cam_vals, 'y_res', 768)

        # Populate the camera values from the saved values.
        self.camera.hflip = self.cam_vals['hflip']
        self.camera.vflip = self.cam_vals['vflip']

        # Set up the default configuration now
        self.set_res(self.cam_vals['x_res'], self.cam_vals['y_res'])
        self.set_exposure_type(self.cam_vals['exposure_type'])
    



    def save_default(self, nv_dict, key, value):
        '''Store a keyvalue pair into a dictionary or SqliteDict in particular
           :param nv_dict:  dictionary name
           :param key:      key
           :param value:    value 
        '''
        if key not in nv_dict:
            nv_dict[key] = value

    ##
    # Return X field of view.  Units are degrees, pixels or ...
    ##
    def getXfov(self, units="pixels"):
        if (units.upper() == 'PIXELS'):
            return 3280
        elif(units.upper() == 'DEGREES'):
            return 62.2
        else:
            return 42

    ##
    # Return Y field of view.  Units are degrees, pixels or ...
    ##
    def getYfov(self, units="pixels"):
        if (units.upper() == 'PIXELS'):
            return 2464
        elif(units.upper() == 'DEGREES'):
            return 48.8
        else:
            return 42

    ##
    # For now just take and set x,y values.  Perhaps later we can do something
    # like ensure that we are only using values that provide the full field of
    # view.  Something like round up/down to nearest value.. but web controls
    # will probably handle that.
    ##
    def set_res(self, x,y):
        logging.debug("set_res, x: %s, y: %s", x, y)
        self.camera.resolution = (x,y)
        self.cam_vals['x_res'] = x
        self.cam_vals['y_res'] = y


    def set_exposure_type(self, type):
        logging.debug("set_exposure_type, type: %s", type)
        self.cam_vals['exposure_type'] = type
        if (type == 'night'):
            self.camera_sleep = 4
            EXPOSURE_FACTOR = 60
            self.camera.iso = 1600 # Actually, it only goes to 800
            # Formerly:
            #self.camera.iso = 20 * EXPOSURE_FACTOR
            self.camera.exposure_mode = 'verylong'
            logging.debug("denominator: %s", EXPOSURE_FACTOR/10)
            logging.debug("framerate: %s", Fraction(1, EXPOSURE_FACTOR/10))
            self.camera.framerate = Fraction(1, EXPOSURE_FACTOR/10)
            self.camera.shutter_speed = EXPOSURE_FACTOR * 100000
            self.camera.annotate_foreground = picamera.Color("white")

        elif (type == 'dusk'):
            ## This stuff has to be tuned in real conditions
            self.camera_sleep = 4
            self.camera_sleep = 0
            EXPOSURE_FACTOR = 20 # 10 gives dusk
            self.camera.iso = 20 * EXPOSURE_FACTOR
            self.camera.exposure_mode = 'verylong'
            logging.debug("denominator: %s", EXPOSURE_FACTOR)
            logging.debug("framerate: %s", Fraction(1, EXPOSURE_FACTOR/10))
            self.camera.framerate = Fraction(1, EXPOSURE_FACTOR/10)
            self.camera.shutter_speed = EXPOSURE_FACTOR * 100000
            self.camera.annotate_foreground = picamera.Color("red")

        else:
            self.camera.iso = 100
            self.camera.exposure_mode = 'auto'
            self.camera.framerate = Fraction(30,1)
            self.camera.shutter_speed = 0  # Auto
            self.camera.annotate_foreground = picamera.Color("black")

    def draw_center(self, filename):

        half_line_len = int(math.sqrt(self.cam_vals['x_res'] * self.cam_vals['y_res']) * 0.0463)

        im = Image.open(filename)
        w, h = im.size
        mw = w/2
        mh = h/2
        draw = ImageDraw.Draw(im)
        draw.line((mw - half_line_len, mh, mw + half_line_len, mh))
        draw.line((mw, mh - half_line_len, mw, mh + half_line_len))
        im.save(filename, format='JPEG', optimize=True)
        
    def snap(self):
        logging.debug("snap: getting ready")
        sleep(self.camera_sleep)

        (tempC, tempF) = self.temp.read_temp()
        temp_str = " %.2fF/%.2fC" % (tempF, tempC)
        date_str = datetime.now().strftime('%Y-%m-%d %H:%M:%S') 
        self.camera.annotate_text = date_str + temp_str

        ##
        # Only open/create temp file here when needed.
        # This is "risky" if /tmp and the final destination image file are on
        # separate file systems.  The create and rename scheme was done to
        # minimize people getting partial files.  If it's moved across file
        # systems, it will be copied.
        ##
        self.tf = tempfile.NamedTemporaryFile(delete=False)

 
        logging.debug("snap: Camera is capturing...")
        self.camera.capture(self.tf.name, format='jpeg')
        logging.debug("snap: Camera is done capturing")

        logging.debug("snap: Drawing center")
        self.draw_center(self.tf.name)
        logging.debug("snap: Done drawing center")

        try:
            os.chmod(self.tf.name, 0644)
            os.rename(self.tf.name,self.cam_vals['PicName'])
        except Exception as e:
            logging.error("Error renaming %s to %s: %s", self.tf.name, self.cam_vals['PicName'], e)
        logging.debug("renamed %s to %s", self.tf.name,self.cam_vals['PicName']) 

if __name__ == '__main__':
    camera = Camera('./test_camera_data.db', '/tmp', 'campic.jpg')

