#!/usr/bin/env python
import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web
import time
import wiringpi
import signal
import sys
from steppermotor import StepperDriver
from irfilter import FilterDriver

from time import sleep

from tornado.util import PY3
if PY3:
    from urllib.parse import urlparse  # py2

    xrange = range
else:
    from urlparse import urlparse  # py3


print "Inspecting..."
print wiringpi.__file__
print "done"

#
# Pin modes.
# 
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

HIGH = 1
LOW = 0

MOV_SPEED = 50
MAX_ROTATE= 55

# This should probably go in the module as it's not going to change much at
# all, but we'll leave it here for now.
Motor1A = 21  # maps to physical pin 29/pi wedge g5
Motor1B = 22  # maps to physical pin 31/pi wedge g6
Motor2A = 23  # maps to physical pin 33/pi wedge g13
Motor2B = 25  # maps to physical pin 37/pi wedge g26

FilterA      = 28  # maps to piwedge G18
FilterB      = 24  # maps to piwedge G19
FilterEnable = 1   # maps to piwedge G20



class WSHandler(tornado.websocket.WebSocketHandler):
    #CORS_ORIGINS = ['localhost', '192.168.1.10', '7.168.1.14']

    def __init__(self, application, request, **kwargs):
        super(WSHandler, self).__init__(application, request, **kwargs)
        print "calling init.."

    def initialize(self, motordriver, filterdriver):
        self.stepper = motordriver
        self.IRfilter = filterdriver
        self.exit_camera = False

    #
    # From
    # http://stackoverflow.com/questions/24800436/under-tornado-v4-websocket-connections-get-refused-with-403
    #
    def check_origin(self, origin):
        """Override to enable support for allowing alternate origins.

        The ``origin`` argument is the value of the ``Origin`` HTTP header,
        the url responsible for initiating this request.

        .. versionadded:: 4.0
        """
        parsed_origin = urlparse(origin)
        origin = parsed_origin.netloc
        origin = origin.lower()

        host = self.request.headers.get("Host")

        if (origin == "71.229.225.0"):
            print "Origin is as expected"
            if (host == "192.168.1.10:8888"):
                print "Host is as expected, returning true"
                return True

        if (origin != host):
            print "Not allowing: origin: " + origin + "host: " + host
        # Check to see that origin matches host directly, including ports
        return True
        return origin == host

    # Check this out:
    # http://stackoverflow.com/questions/24851207/tornado-403-get-warning-when-opening-websocket
    #def check_origin(self, origin):
    #    parsed_origin = urlparse(origin)
    #    # parsed_origin.netloc.lower() gives localhost:3333
    #    print "parsed_origin.hostname: " + parsed_origin.hostname
    #    return parsed_origin.hostname in self.CORS_ORIGINS

    def open(self):
        print 'Connected.\n' 

    def on_message(self, message):
        print 'received message: %s\n' %message
        data = message.split(";")
        # TODO: Verify values before enabling motor.
        self.stepper.motor_enable()
        for item in data:
            try:
                (type,strval) = item.split(":")
            except ValueError:
                print "Unparseable message: " + message
                print "Syntax: X:xval[;Y:yval[;M:message]]"
                return

            if (type.upper() == "X"):
                print "rotating " + strval + "degrees"
                try:
                    degrees = float(strval)
                except:
                    print "Unexpected string representation of number: " + strval
                    return
                idegrees = int(degrees)
                print "Moving by " + str(idegrees)
                #
                # Ignore requests greater than MAX_ROTATE degrees.
                # This keeps people from just spinning the thing as a prank.
                if (idegrees <= MAX_ROTATE):
                    self.stepper.rotate(int(degrees), MOV_SPEED)
            elif (type.upper() == "Y"):
                print "looking at the sky by " + strval + "units"
            elif (type.upper() == "F"):
                print "IRfilter " + strval
                if (strval.upper() == "IN"):
                    print "putting filter in"
                    self.IRfilter.filterIn()
                else:
                    print "taking filter out"
                    self.IRfilter.filterOut()
            elif (type.upper() == "M"):
                print "Message: " + strval

        self.stepper.cleanup()

    def on_close(self):
        print 'connection closed\n'

#    def __del__(self):
#        self.exit_camera = True

stepperDriver = StepperDriver(Motor1A, Motor1B, Motor2A, Motor2B)
IRfilterDriver = FilterDriver(FilterA, FilterB, FilterEnable)

application = tornado.web.Application([
              (r'/ws', WSHandler, dict(motordriver=stepperDriver,
              filterdriver=IRfilterDriver)),
              ])

if __name__ == "__main__":
  http_server = tornado.httpserver.HTTPServer(application)
  http_server.listen(8888)
  main_loop = tornado.ioloop.IOLoop.instance()
  main_loop.start()

