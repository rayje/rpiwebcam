#!/bin/bash

if [ $(id -u) != 0 ]; then
  echo "$0 must be run as root"
  exit 1
fi

cd /home/pi/webcam

trap 'echo jobs: $(jobs -pr); sudo kill $(jobs -pr); exit' EXIT SIGINT
trap 'echo "killing, restarting"; sudo kill $(jobs -pr); startjobs' HUP

startjobs(){
    echo "Starting jobs, parameter: $1"
#    sudo python -u camera.py $1 > cam.log & 2>&1
    if [ -e tornado.log ]; then
        mv tornado.log tornado.log.$(date +%s)
    fi
    sudo python -u webmotion.py > tornado.log & 2>&1
}

killjobs(){
    for job in $(ps auxw |grep [w]ebmo |awk '{print $2}'); do 
        sudo kill $job 
    done
}


startjobs $1
echo "jobs -pr: " $(jobs -pr)
#while :; do
    wait
#    sleep 1
#done


