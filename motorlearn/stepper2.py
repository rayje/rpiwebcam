#!/usr/bin/env python
import wiringpi
import signal
import sys
from steppermotor import StepperDriver

from time import sleep




# Not sure if I should do this here or in the module.
# Will do it in the module for now.
#wiringpi.wiringPiSetup()

#
# Pin modes.
# 
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

HIGH = 1
LOW = 0

#
# Motor control is handled by a salvaged FT5754M. 
# Data sheet:
# http://www.elemar.pl/PDF/FT57xx.pdf
# For our wiring, the left half of the FT5754M controls one phase and
# the right half controls the other phase
# The 5754 pins are as follows:
# 1 - array 1, pin 2 controller
# 2 - Connects to one of the phase wires, red
# 3 - Connects to positive and the center tap of one phase, black.
# 4 - Connects to the other phase wire, brown
# 5 - Array 1, pin 4 controller
# 6 - Array 1, motor ground.  Also connected to rpi ground
# 7 - Array 2, motor ground.  Also connected to rpi ground
# 8 - Array 2, pin 9 controller
# 9 - Connects to one of the phase wires, yellow
# 10 - Connects to positive and the center tap of one phase, orange.
# 11 - Connects to the other phase wire, green
# 12 - Array 2, pin 11 controller
#
# The RPi GPIO pins are connected to pins 1,5, 8, 12 as 
# follows:
# GPIO Phys wPi  connected to 5754 pin
# 5    29   21                1
# 6    31   22                5
# 13   33   23                8
# 26   37   25                12
#
# The plan I think is to energize the stepper motor 
# 2 - Connects to one of the phase wires, red
# 3 - Connects to positive and the center tap of one phase, black.
# 4 - Connects to the other phase wire, brown
#
# Pin mapping between physical, BCM_CPIO, and wiring pi is shown by calling gpio:
# gpio readall
#
# Pasted in for clutter's sake:
# +-----+-----+---------+------+---+---Pi 3---+---+------+---------+-----+-----+
# | BCM | wPi |   Name  | Mode | V | Physical | V | Mode | Name    | wPi | BCM |
# +-----+-----+---------+------+---+----++----+---+------+---------+-----+-----+
# |     |     |    3.3v |      |   |  1 || 2  |   |      | 5v      |     |     |
# |   2 |   8 |   SDA.1 | ALT0 | 1 |  3 || 4  |   |      | 5V      |     |     |
# |   3 |   9 |   SCL.1 | ALT0 | 1 |  5 || 6  |   |      | 0v      |     |     |
# |   4 |   7 | GPIO. 7 |   IN | 0 |  7 || 8  | 1 | ALT5 | TxD     | 15  | 14  |
# |     |     |      0v |      |   |  9 || 10 | 1 | ALT5 | RxD     | 16  | 15  |
# |  17 |   0 | GPIO. 0 |   IN | 0 | 11 || 12 | 0 | ALT0 | GPIO. 1 | 1   | 18  |
# |  27 |   2 | GPIO. 2 |   IN | 0 | 13 || 14 |   |      | 0v      |     |     |
# |  22 |   3 | GPIO. 3 |   IN | 0 | 15 || 16 | 0 | IN   | GPIO. 4 | 4   | 23  |
# |     |     |    3.3v |      |   | 17 || 18 | 0 | IN   | GPIO. 5 | 5   | 24  |
# |  10 |  12 |    MOSI | ALT0 | 0 | 19 || 20 |   |      | 0v      |     |     |
# |   9 |  13 |    MISO | ALT0 | 0 | 21 || 22 | 0 | IN   | GPIO. 6 | 6   | 25  |
# |  11 |  14 |    SCLK | ALT0 | 0 | 23 || 24 | 1 | OUT  | CE0     | 10  | 8   |
# |     |     |      0v |      |   | 25 || 26 | 1 | OUT  | CE1     | 11  | 7   |
# |   0 |  30 |   SDA.0 |   IN | 1 | 27 || 28 | 1 | IN   | SCL.0   | 31  | 1   |
# |   5 |  21 | GPIO.21 |   IN | 1 | 29 || 30 |   |      | 0v      |     |     |
# |   6 |  22 | GPIO.22 |   IN | 1 | 31 || 32 | 0 | IN   | GPIO.26 | 26  | 12  |
# |  13 |  23 | GPIO.23 |   IN | 0 | 33 || 34 |   |      | 0v      |     |     |
# |  19 |  24 | GPIO.24 | ALT0 | 0 | 35 || 36 | 0 | IN   | GPIO.27 | 27  | 16  |
# |  26 |  25 | GPIO.25 |   IN | 0 | 37 || 38 | 0 | ALT0 | GPIO.28 | 28  | 20  |
# |     |     |      0v |      |   | 39 || 40 | 0 | ALT0 | GPIO.29 | 29  | 21  |
# +-----+-----+---------+------+---+----++----+---+------+---------+-----+-----+
# | BCM | wPi |   Name  | Mode | V | Physical | V | Mode | Name    | wPi | BCM |
# +-----+-----+---------+------+---+---Pi 3---+---+------+---------+-----+-----+
#
# 

# 
# Stepper motor pins from 
# http://homepage.divms.uiowa.edu/~jones/step/types.html
# The motor being used is a two phase unipolar stepper motor.  
# Each phase is divided into two parts, A, B.
# We are going to try to produce output like this:
#
#  Winding 1a 1100110011001100110011001
#  Winding 1b 0011001100110011001100110
#  Winding 2a 0110011001100110011001100
#  Winding 2b 1001100110011001100110011
#             time --->
#
Motor1A = 21  # maps to physical pin 29/pi wedge g5
Motor1B = 22  # maps to physical pin 31/pi wedge g6
Motor2A = 23  # maps to physical pin 33/pi wedge g13
Motor2B = 25  # maps to physical pin 37/pi wedge g26

stepper = StepperDriver(Motor1A, Motor1B, Motor2A, Motor2B)
print "Full step"
#stepper.full_step(50)
#stepper.cleanup()
#exit()

stepper.motor_enable()
for i in range(1,250):
    print "Step " + str(i)
    stepper.tiny_step(10)
#    stepper.full_step(100)
print "Shutting down motor"
stepper.cleanup()
sleep(1)
print "Single tiny step"
stepper.motor_enable()
stepper.tiny_step(10)
