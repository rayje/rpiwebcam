#!/usr/bin/env python
import wiringpi
import signal
import sys
from time import sleep

def cleanup():
    wiringpi.digitalWrite(Motor1A, LOW)
    wiringpi.pinMode(Motor1A, INPUT)

    wiringpi.digitalWrite(Motor1A, LOW)
    wiringpi.pinMode(Motor1B, INPUT)

    wiringpi.pwmWrite(Motor1E, LOW)
    wiringpi.pinMode(Motor1E, INPUT)

wiringpi.wiringPiSetup()
#wiringpi.wiringPiSetupPhys()

#
# Pin modes.
# 
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

HIGH = 1
LOW = 0

PWM_OFF = 0
PWM_HALF = 512
PWM_FULL = 1024
PWM_75 = int(1024 * .75)

#
# Physical pin 12, BCM_GPIO 18, wPi pin 1, supports hardware pwm.
#
#
Motor1A = 4  # maps to physical pin 16
Motor1B = 5  # maps to physical pin 18
Motor1E = 1  # maps to physical pin 12

PIN_TO_PWM = Motor1E

wiringpi.pinMode(Motor1A, OUTPUT)
wiringpi.pinMode(Motor1B, OUTPUT)
wiringpi.pinMode(Motor1E, PWM)
wiringpi.pwmWrite(Motor1E, PWM_OFF)

print "Turning motor on"
wiringpi.digitalWrite(Motor1A, HIGH)
wiringpi.digitalWrite(Motor1B, LOW)
wiringpi.pwmWrite(Motor1E, PWM_FULL)
sleep(10)
print "Half speed now"
wiringpi.pwmWrite(Motor1E, PWM_HALF)
sleep(10)
print "3/4 speed now"
wiringpi.pwmWrite(Motor1E, PWM_75)
sleep(10)

print "Stopping motor"

wiringpi.pwmWrite(Motor1E, PWM_OFF)
sleep(2)
cleanup()
exit()


print "Reversing motor"
sleep (0.5)
wiringpi.digitalWrite(Motor1A, LOW)
wiringpi.digitalWrite(Motor1B, HIGH)
wiringpi.digitalWrite(Motor1E, HIGH)

sleep(2)
print "Stopping motor"

wiringpi.digitalWrite(Motor1E, LOW)
sleep (0.5)
wiringpi.digitalWrite(Motor1E, HIGH)
sleep (0.5)
wiringpi.digitalWrite(Motor1E, LOW)
sleep (0.5)
wiringpi.digitalWrite(Motor1E, HIGH)
sleep (0.5)

print "Trying motor speed control"

wiringpi.digitalWrite(Motor1A, HIGH)
wiringpi.digitalWrite(Motor1B, LOW)

wiringpi.softPwmCreate(PIN_TO_PWM, 0, 100)

for speed in range(0,100):
    wiringpi.softPwmWrite(PIN_TO_PWM, speed)
    wiringpi.delay(30)
sleep(1)
for speed in reversed(range(0,100)):
    wiringpi.softPwmWrite(PIN_TO_PWM, speed)
    wiringpi.delay(30)

sleep(1)
print "Shutting down motor"
wiringpi.digitalWrite(Motor1E, LOW)
wiringpi.digitalWrite(Motor1A, LOW)
wiringpi.digitalWrite(Motor1B, LOW)
