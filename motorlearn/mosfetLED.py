#!/usr/bin/env python
import time
import wiringpi
import signal
import sys

from time import sleep



#
# Pin modes.
# 
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

HIGH = 1
LOW = 0

#
# Pull up/down modes
#
PUD_DIS  = 0
PUD_DOWN = 1
PUD_UP   = 2

#
# Pull up/down modes
#
PUD_DIS  = 0
PUD_DOWN = 1
PUD_UP   = 2

PWM_MODE_MS = 0
pin = 3
wiringpi.wiringPiSetup()

def init(pin):
    wiringpi.pinMode(pin, OUTPUT)
    wiringpi.pullUpDnControl(pin, PUD_UP)
    wiringpi.digitalWrite(pin, HIGH)

def shutdown(pin):
    wiringpi.pinMode(pin, OUTPUT)
    wiringpi.pullUpDnControl(pin, PUD_DOWN)
    wiringpi.digitalWrite(pin, LOW)

DELAY=30
init(pin)
sleep (2)
shutdown(pin)

exit()
