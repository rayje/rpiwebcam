#!/usr/bin/env python
import wiringpi
import signal
import sys
from time import sleep

#wiringpi.wiringPiSetup()
wiringpi.wiringPiSetupPhys()

#
# Pin modes.
# Physical pin 12, BCM_GPIO 18, wPi pin 1 supports hardware pwm.
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

HIGH = 1
LOW = 0

Motor1A = 4  # maps to physical pin 16
Motor1B = 5  # maps to physical pin 18
Motor1E = 6  # maps to physical pin 22
Motor1A = 16  # physical pin 16
Motor1B = 18  # physical pin 18
Motor1E = 22  # physical pin 22
PIN_TO_PWM = Motor1E

wiringpi.pinMode(Motor1A, OUTPUT)
wiringpi.pinMode(Motor1B, OUTPUT)
wiringpi.pinMode(Motor1E, OUTPUT)

print "Turning motor on"
wiringpi.digitalWrite(Motor1A, HIGH)
wiringpi.digitalWrite(Motor1B, LOW)
wiringpi.digitalWrite(Motor1E, HIGH)

sleep(2)
print "Stopping motor"

wiringpi.digitalWrite(Motor1E, LOW)

print "Reversing motor"
sleep (0.5)
wiringpi.digitalWrite(Motor1A, LOW)
wiringpi.digitalWrite(Motor1B, HIGH)
wiringpi.digitalWrite(Motor1E, HIGH)

sleep(2)
print "Stopping motor"

wiringpi.digitalWrite(Motor1E, LOW)
sleep (0.5)
wiringpi.digitalWrite(Motor1E, HIGH)
sleep (0.5)
wiringpi.digitalWrite(Motor1E, LOW)
sleep (0.5)
wiringpi.digitalWrite(Motor1E, HIGH)
sleep (0.5)

print "Trying motor speed control"

wiringpi.digitalWrite(Motor1A, HIGH)
wiringpi.digitalWrite(Motor1B, LOW)

wiringpi.softPwmCreate(PIN_TO_PWM, 0, 100)

for speed in range(0,100):
    wiringpi.softPwmWrite(PIN_TO_PWM, speed)
    wiringpi.delay(30)
sleep(1)
for speed in reversed(range(0,100)):
    wiringpi.softPwmWrite(PIN_TO_PWM, speed)
    wiringpi.delay(30)

sleep(1)
print "Shutting down motor"
wiringpi.digitalWrite(Motor1E, LOW)
wiringpi.digitalWrite(Motor1A, LOW)
wiringpi.digitalWrite(Motor1B, LOW)
