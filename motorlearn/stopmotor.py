#!/usr/bin/env python
import RPi.GPIO as GPIO
import signal
import sys
import time

GPIO.setmode(GPIO.BOARD)

Motor1A = 16
Motor1B = 18
Motor1E = 22

GPIO.setup(Motor1A, GPIO.OUT)
GPIO.setup(Motor1B, GPIO.OUT)
GPIO.setup(Motor1E, GPIO.OUT)

print "Turning motor off"
GPIO.output(Motor1E, GPIO.LOW)
print "Stopping motor"


GPIO.cleanup()

