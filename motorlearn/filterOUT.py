#!/usr/bin/env python
#
# Help from
# https://business.tutsplus.com/tutorials/controlling-dc-motors-using-python-with-a-raspberry-pi--cms-20051
#
# Rpi GPIO/wiring pi/piwedge mapping can be obtained with the 
# gpio readall 
# command.  The piwedge uses the BCM pin numbers and wiring pi uses wPi
# (surprise)
# For example:
# Motor1A = 21  # wiring pi 21, maps to physical pin 29/pi wedge g5
#
# This is using a SN754410 motor controller to enable and disable an IR cut
# filter.  It's got a solenoid so it's similar to a motor.  We'll just give it
# the minimum pulse length to make it work.
# 
# SN754410 to RPi pin connections:
# 1: Enable output on high: G18 on piwedge, wiring pi pin 1,  tutorial is GPIO25/Phys 22
# 2: driver input:          G19 on piwedge, wiring pi pin 24, tutorial is GPIO24/Phys 18
# 3: cut filter wire one
# 4: Ground
# 5: Ground
# 6: cut filter wire one
# 7: driver input:          G20 on piwedge, wiring pi pin 28, tutorial is GPIO23/Phys 16
# 12: Ground
# 13: Ground
# 8: Vcc, 5v power for solenoid of Rpi
# 16: Vcc, 5v logic power supply of Rpi
# 
#
import wiringpi
import signal
import sys
from time import sleep

def cleanup():
    wiringpi.digitalWrite(Motor1A, LOW)
    wiringpi.pinMode(Motor1A, INPUT)

    wiringpi.digitalWrite(Motor1A, LOW)
    wiringpi.pinMode(Motor1B, INPUT)

    wiringpi.pwmWrite(Motor1E, LOW)
    wiringpi.pinMode(Motor1E, INPUT)

wiringpi.wiringPiSetup()
#wiringpi.wiringPiSetupPhys()

#
# Pin modes.
# 
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

HIGH = 1
LOW = 0

PWM_OFF = 0
PWM_HALF = 512
PWM_FULL = 1024
PWM_75 = int(1024 * .75)

#
# Physical pin 12, BCM_GPIO 18, wPi pin 1, supports hardware pwm.
#
#
Motor1A = 28  # maps to piwedge G18
Motor1B = 24  # maps to piwedge G19
Motor1E = 1   # maps to piwedge G20


wiringpi.pinMode(Motor1A, OUTPUT)
wiringpi.pinMode(Motor1B, OUTPUT)
wiringpi.pinMode(Motor1E, OUTPUT)

print "Turning motor on"
wiringpi.digitalWrite(Motor1A, HIGH)
wiringpi.digitalWrite(Motor1B, LOW)
wiringpi.digitalWrite(Motor1E, HIGH)
wiringpi.delay(20)
wiringpi.digitalWrite(Motor1E, LOW)

#sleep(5)
#print "Going Backwards"
#wiringpi.digitalWrite(Motor1A, LOW)
#wiringpi.digitalWrite(Motor1B, HIGH)
#wiringpi.digitalWrite(Motor1E, HIGH)
#wiringpi.delay(20)
#wiringpi.digitalWrite(Motor1E, LOW)

#sleep (5)
print "Stopping"
wiringpi.digitalWrite(Motor1E, LOW)
cleanup()
exit()
