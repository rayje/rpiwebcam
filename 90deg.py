#!/usr/bin/env python
import time
import wiringpi
import signal
import sys
from steppermotor import StepperDriver

from time import sleep



#
# Pin modes.
# 
#
INPUT  = 0
OUTPUT = 1
PWM    = 2 

HIGH = 1
LOW = 0

# This should probably go in the module as it's not going to change much at
# all, but we'll leave it here for now.
Motor1A = 21  # maps to physical pin 29/pi wedge g5
Motor1B = 22  # maps to physical pin 31/pi wedge g6
Motor2A = 23  # maps to physical pin 33/pi wedge g13
Motor2B = 25  # maps to physical pin 37/pi wedge g26


if (len(sys.argv) > 1):
    START_POS=sys.argv[1]
else:
    START_POS="0"

print "Start_pos: " + START_POS


stepper = StepperDriver(Motor1A, Motor1B, Motor2A, Motor2B, step_state=START_POS)

DELAY=30
ITER=90

stepper.rotate(90, 70)

stepper.motor_enable()
stepper.go_reverse()
for i in range (0, ITER):
    stepper.tiny_step (50)
    wiringpi.delay(DELAY)

stepper.motor_disable()
#sleep(5)
exit()

for i in range (0, 5):
    stepper.full_step(10)
    sleep (1)


for i in range (0, 5):
    stepper.full_step(80)
    sleep (1)

stepper.rotate(90, 50)

exit()

stepper.rotate(180,50)
sleep (1)
stepper.rotate(-180,50)
exit()
sleep (2)
stepper.go_reverse()
stepper.rotate(45,50)
sleep (2)
stepper.go_forward()
stepper.rotate(30,50)
sleep (2)
stepper.go_reverse()
stepper.rotate(33,50)

